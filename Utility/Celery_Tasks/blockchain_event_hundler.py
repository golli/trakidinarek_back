from celery import task
import time

from web3 import Web3, HTTPProvider

w3 = Web3(HTTPProvider('http://192.168.3.131:8545'))


def handle_event(event):
    print(w3.toHex(event))
    print(w3.toText(w3.toHex(event)))


def log_loop(event_filter, poll_interval):
    while True:
        for event in event_filter.get_new_entries():
            handle_event(event)
        time.sleep(poll_interval)

@task()
def handle_events():
    block_filter = w3.eth.filter('latest')
    log_loop(block_filter, 2)
