import requests
from django.core.cache import cache

from Utility.Celery_Tasks.blockchain_event_hundler import handle_events


def one_time_startup():

        response = requests.post('http://localhost/wp-json/jwt-auth/v1/token', json={'username': 'TrakiDinarekAdmin',
                                                                                     'password': 'omLuJD^EfDgki3('
                                                                                                 'nCYQRT$pD'})
        data = response.json()
        cache.set('token', data['token'], None)
        handle_events.delay()
        # print(cache.get('token'))
#./ngrok http 8000