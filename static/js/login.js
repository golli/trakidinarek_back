(function ($) {
//login Facebook
    window.fbAsyncInit = function() {
        FB.init({
          appId      : '2291569857798926',
          xfbml      : true,
          version    : 'v3.2'
        });
        if (logout==="True"){
            logoutFB();
        }
    };
    (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "https://connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    function statusChangeCallback(response) {
        $.ajaxSetup({
         beforeSend: function(xhr, settings) {
             function getCookie(name) {
                 var cookieValue = null;
                 if (document.cookie && document.cookie !== '') {
                     var cookies = document.cookie.split(';');
                     for (var i = 0; i < cookies.length; i++) {
                         var cookie = jQuery.trim(cookies[i]);
                         // Does this cookie string begin with the name we want?
                         if (cookie.substring(0, name.length + 1) === (name + '=')) {
                             cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                             break;
                         }
                     }
                 }
                 return cookieValue;
             }
             if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                 // Only send the token to relative URLs i.e. locally.
                 xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
             }
         }
        });

        $.ajax({
           type: "POST",
           dataType: "json",
           data:response['authResponse'],
           url: "/authenticateFacebook/",
           success: function(data){
               if (data['success']) {
                   window.location=data.url
                   return false


               }
           }
        });

    }
    $("#loginFB").click(function () {
             FB.login(function(response) {
          // handle the response
          statusChangeCallback(response);

        }, {scope: 'public_profile,email,user_likes'});
    });

     function logoutFB(){
        FB.logout(function (response) {
            // Person is now logged out
        });
    };
    $(".btn-show-pass").click(function () {

    var x = document.getElementById("id_password");
    if (x.type == "password") {
            x.type = "text";
        } else {
            x.type = "password";
    }
    });
    $body = $("body");

    $(document).on({
        ajaxStart: function() { $body.addClass("loading");    },
         ajaxStop: function() { $body.removeClass("loading"); }
    });
//login form
 $.validator.addMethod("password_rules", function(value) {
       return /^[A-Za-z0-9\d=!\-@._*]*$/.test(value) // consists of only these
           && /[a-z]/.test(value) // has a lowercase letter
           && /\d/.test(value) // has a digit
    });
 $("#loginForm").validate({
    errorClass: "error-message",
    errorElement:"div",
    messages:{
        email:{
            required:"Please enter you email",
            email:"Please provide a valid email"
        },
        password: {
            required: "Please provide a password",
            minlength: "Your password must be at least 8 characters long",
            password_rules: "The password should meet some minimum requirements, at least one lower-case " +
                "character, at least one digit, Allowed Characters: A-Z a-z 0-9 @ * _ - . !"
        }
    },
    rules: {
        email: {
            required: true,
            email: true
        },
        password: {
            required: true,
            minlength: 8,
            password_rules: true
        }
    },
      submitHandler: function(form) {
    // do other things for a valid formm
          console.log("submitted");
          form.submit();
      }
 });
})(jQuery);



