(function ($) {
    $.validator.addMethod("phoneRegex", function(phone_number, element) {
            phone_number = phone_number.replace(/\s+/g, "");
            return this.optional(element) || phone_number.length > 9 &&
            phone_number.match(/^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/);
        }, "<br />Please specify a valid phone number");
    $.validator.addMethod("password_rules", function(value) {
        return /^[A-Za-z0-9\d=!\-@._*]*$/.test(value) // consists of only these
           && /[a-z]/.test(value) // has a lowercase letter
           && /\d/.test(value) // has a digit
    });
    $.validator.addMethod('file_size', function (value, element, param) {
        return this.optional(element) || (element.files[0].size <= param)
    }, 'File size must be less than 5Mo');

    let form = $(".form-horizontal");
    form.validate({ errorElement:"p",
                    errorClass:"help-block",
                    highlight: function(element) {
                         $(element).parents("div.form-group").addClass("has-error").removeClass("has-success");
                    },
                    unhighlight: function(element) {
                         $(element).parents("div.form-group").removeClass("has-error").addClass("has-success");
                    },
                    messages: {
                        first_name: "Please enter your first name, at least 3 characters",
                        last_name: "Please enter you last name, at least 3 characters",
                        email:{
                            required:"Please enter you email",
                            email:"Please provide a valid email"
                        },
                        password: {
                            required: "Please provide a password",
                            minlength: "Your password must be at least 8 characters long",
                            password_rules: "The password should meet some minimum requirements, at least one lower-case " +
                                "character, at least one digit, Allowed Characters: A-Z a-z 0-9 @ * _ - . !"

                        },
                        confirm_password: {
                            equalTo: "Please confirm your password"
                        },
                        phone_number: "Please enter a valid phone number",

                    },
                    rules: {
                        first_name: {
                            required: true,
                            minlength: 3
                        },
                        last_name: {
                            required: true,
                            minlength: 3
                        },
                        password: {
                            required: true,
                            minlength: 8,
                            password_rules: true
                        },
                        confirm_password: {
                            required: true,
                            minlength: 8,
                            equalTo: ("#id_password"),
                            password_rules: true
                        },
                        email: {
                            required: true,
                            email: true
                        },
                        phone_number: {
                            phoneRegex:true

                        },
                        president_justification:{
                            extension:"jpg,jpeg,pdf,png",
                            file_size:"5242880"
                        },
                        jort_justification:{
                            extension:"jpg,jpeg,pdf,png",
                            file_size:"5242880"
                        },
                        declaration_justification:{
                            extension:"jpg,jpeg,pdf,png",
                            file_size:"5242880"
                        }


                    }
                });
       $(".nav-tabs li").click(function(){
              if (localStorage)
              localStorage['nav_tab'] = $( ".nav-tabs li" ).index( this );
              active_tab=$( ".nav-tabs li" ).index( this );
   });
    if (active_tab>1){
            $(".nav-tabs li:eq("+0+")").addClass("active");
            $(".tab-pane:eq("+0+")").addClass("active");
    }
    else {
            $(".nav-tabs li:eq("+active_tab+")").addClass("active");
            $(".tab-pane:eq("+active_tab+")").addClass("active");
    }
    $(".image-checkbox").each(function () {
      if ($(this).find('input[type="checkbox"]').first().attr("checked")) {
        $(this).addClass('image-checkbox-checked');
      }
      else {
        $(this).removeClass('image-checkbox-checked');
      }
    });

    // sync the state to the input
    $(".image-checkbox").on("click", function (e) {
      $(this).toggleClass('image-checkbox-checked');
      var $checkbox = $(this).find('input[type="checkbox"]');
      $checkbox.prop("checked",!$checkbox.prop("checked"))

      e.preventDefault();
    });

})(jQuery);
