function my_special_notification_callback(data) {
    var list='';
    for (var i=0; i < data.unread_list.length; i++) {
        msg = data.unread_list[i];
        list= list+'<li >' +
                        '<a href="#">'+
                            '<span class="label label-warning"><i class="fa fa-bell"></i></span>'+'user '+
                                  msg['actor_object_id']+': '+
                                  msg.verb +
                            '<span class="small italic">'+msg.timestamp+'</span>' +
                        '</a>' +
                    '</li>' ;
    }
            document.getElementById("unread_notifications").innerHTML=list;

}