(function ($) {
    	let form = $("#msform");
		var validator = form.validate({
				errorElement:"span",
				errorClass:"help-block",
				highlight: function(element) {
					 $(element).parents("div.form-group").addClass("has-error").removeClass("has-success");
				},
				unhighlight: function(element) {
					 $(element).parents("div.form-group").removeClass("has-error").addClass("has-success");
				},
      			ignore: ":hidden:not(.summernote),.note-editable.panel-body",
				messages: {
					bottlenecks: {required : "Please enter your first name"},
					title: {required :"Please enter a name to your project"},
					subtitle:{required : "Please enter a clear, brief description that helps people quickly understand the gist of your project."},
					Project_image:{
						required:"Please add a project image",
						extension:"Please choose a valid image",
						filesize:"File size must be less than 250 mb"

					},
					Project_video: {
						extension:"Please choose a valid video",
						filesize:"File size must be less than 500 mb"

					},
					location: {
						required : "Please enter the location of your project"
					},
					Description: "Please enter a good description to your project",

				},
				rules: {
					title: {
						required: true,
						minlength: 20
					},
					subtitle: {
						required: true,
						minlength: 30
					},
					Project_image: {
						required: true,
						extension:"jpg,jpeg,png,git",
						filesize:104857600

					},
					Project_video: {
						required: false,
						extension:"MOV,MPEG,AVI,MP4,3GP,MWMV,FLV",
						filesize:429916160
					},
					location:{
						required: true,
						minlength: 20

					},
					Description:{
						required: true,
					},
					bottlenecks:{
						required: true,
						minlength: 50
					}

				}

            });

	$('#msform input').on('keyup blur', function () { // fires on every keyup & blur
		validator.element(this);

	});

	var limit = 2;
	// init the state from the input
	$(".image-checkbox").each(function () {
	  if ($(this).find('input[type="checkbox"]').first().attr("checked")) {
		$(this).addClass('image-checkbox-checked');
	  }
	  else {
		$(this).removeClass('image-checkbox-checked');
	  }
	});
	// sync the state to the input
	$(".image-checkbox").on("click", function (e) {
		let checked = $(this).hasClass('image-checkbox-checked');
		if ($('.categories_list').filter(':checked').length>=limit && !checked){
			return;
		}
		$(this).toggleClass('image-checkbox-checked');
		let $checkbox = $(this).find('input[type="checkbox"]');
		$checkbox.prop("checked",!$checkbox.prop("checked"));

		e.preventDefault();
		let checkedNum = $('.categories_list').filter(':checked').length;
		if (checkedNum == 1) {
		  // User didn't check any checkboxes
		   $(':input[name="next"]').prop('disabled', false);

		}
		else if (checkedNum==0){
		   $(':input[name="next"]').prop('disabled', true);

		}
	});
	//validaton rules
	$.validator.addMethod('filesize', function (value, element, param) {
		return this.optional(element) || (element.files[0].size <= param)
	});


	//jQuery time
	var current_fs, next_fs, previous_fs; //fieldsets
	var left, opacity, scale; //fieldset properties which we will animate
	var animating; //flag to prevent quick multi-click glitches

	$(".next").click(function(){

		current_fs = $(this).parents("fieldset");
		next_fs = $(this).parents("fieldset").next();
		if (!current_fs.is("#checks")){
			if (form.valid()===true){

			}else {
				return ;
			}

		}
		if(animating) return false;
		animating = true;

		//activate next step on progressbar using the index of next_fs
		$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

		//show the next fieldset
		next_fs.show();
		//hide the current fieldset with style
		current_fs.animate({opacity: 0}, {
			step: function(now, mx) {
				//as the opacity of current_fs reduces to 0 - stored in "now"
				//1. scale current_fs down to 80%
				scale = 1 - (1 - now) * 0.2;
				//2. bring next_fs from the right(50%)
				left = (now * 50)+"%";
				//3. increase opacity of next_fs to 1 as it moves in
				opacity = 1 - now;
				current_fs.css({
			'transform': 'scale('+scale+')',
		  });
				next_fs.css({'left': left, 'opacity': opacity});
			},
			duration: 800,
			complete: function(){
				current_fs.hide();
				animating = false;
			},
			//this comes from the custom easing plugin
			easing: 'easeInOutBack'
		});
		localStorage.setItem('saved_div',$('.main-content').html());


	});

	$(".previous").click(function(){
		if(animating) return false;
		animating = true;

		current_fs = $(this).parents("fieldset");
		previous_fs = $(this).parents("fieldset").prev();

		//de-activate current step on progressbar
		$("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active")	;

		//show the previous fieldset
		previous_fs.show();
		//hide the current fieldset with style
		current_fs.animate({opacity: 0}, {
			step: function(now, mx) {
				//as the opacity of current_fs reduces to 0 - stored in "now"
				//1. scale previous_fs from 80% to 100%
				scale = 0.8 + (1 - now) * 0.2;
				//2. take current_fs to the right(50%) - from 0%
				left = ((1-now) * 50)+"%";
				//3. increase opacity of previous_fs to 1 as it moves in
				opacity = 1 - now;
				current_fs.css({'left': left});
				previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
			},
			duration: 800,
			complete: function(){
				current_fs.hide();
				animating = false;
			},
			//this comes from the custom easing plugin
			easing: 'easeInOutBack'
		});
	});
    $(document).ready(function() {
        $.validator.addMethod("greater_than", function(value, element) {
            var campain_end_date = $('#id_Campaign_duration').val();
            return Date.parse(campain_end_date) <= Date.parse(value) || value == "";
        }, "* End date must be after start date");
        $('#formId').validate();
    });

	$(".submit").click(function(){
		let form = $("#msform");

		 form.validate({
				errorElement:"span",
				errorClass:"help-block",
				highlight: function(element) {
					 $(element).parents("div.form-group").addClass("has-error").removeClass("has-success");
				},
				unhighlight: function(element) {
					 $(element).parents("div.form-group").removeClass("has-error").addClass("has-success");
				},
      			ignore: ":hidden:not(.summernote),.note-editable.panel-body",
				messages: {
					bottlenecks: {required : "Please enter your first name"},
					title: {required :"Please enter a name to your project"},
					subtitle:{required : "Please enter a clear, brief description that helps people quickly understand the gist of your project."},
					Project_image:{
						required:"Please add a project image",
						extension:"Please choose a valid image",
						filesize:"File size must be less than 250 mb"

					},
					Project_video: {
						extension:"Please choose a valid video",
						filesize:"File size must be less than 500 mb"

					},
					location: {
						required : "Please enter the location of your project"
					},
					Description: "Please enter a good description to your project",

				},
				rules: {
					title: {
						required: true,
						minlength: 20
					},
					subtitle: {
						required: true,
						minlength: 30
					},
					Project_image: {
						required: true,
						extension:"jpg,jpeg,png,git",
						filesize:104857600

					},
					Project_video: {
						required: false,
						extension:"MOV,MPEG,AVI,MP4,3GP,MWMV,FLV",
						filesize:429916160
					},
					location:{
						required: true,
						minlength: 10

					},
					Description:{
						required: true,
					},
					bottlenecks:{
						required: true,
						minlength: 50
					},
					Funding_goal: {
						required: false,
						minValue:1,
						maxValue:1000000

					},
					Campaign_duration : {
						required:false
					},
					starting_date:{
						greaterThan:"#id_Campaign_duration"
					}


				}

            });
	});


window.addEventListener("beforeunload", function (e) {
  saveFormData();

  (e || window.event).returnValue = null;
  return null;
});

	function saveFormData() {
			localStorage.setItem('saved_div',$('.main-content').html());
	}


function initialize() {
  var input = document.getElementById('id_location');
  var autocomplete =new google.maps.places.Autocomplete(input);
   // Set initial restrict to the greater list of countries.
	autocomplete.setComponentRestrictions(
		{'country': ['tn']});
    autocomplete.addListener('place_changed', fillInAddress);
	function fillInAddress() {
		// Get the place details from the autocomplete object.
		  var place = autocomplete.getPlace();
		  document.getElementById('id_lat').value=place.geometry.location.lat();
          document.getElementById('id_lon').value=place.geometry.location.lng();
		validator.element(input);

  			console.log(place);
	}

}

google.maps.event.addDomListener(window, 'load', initialize);
$('input[type=radio][name=Funding_needed]').change(function() {
    if (this.value == 'social_action') {
		$('.id').hide();
    }
    else {
		$('.id').show();

    }
});


})(jQuery);
