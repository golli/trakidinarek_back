  /* Formating function for row details */
    function fnFormatDetails(oTable, nTr) {
      var aData = oTable.fnGetData(nTr);
      var sOut = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">';
      sOut += '<tr><td>Association Name:</td><td>' + aData[7] + '</td></tr>';
      sOut += '<tr><td>Identity file :</td><td>'+aData[8]+'</td></tr>';
      sOut += '<tr><td>ASSOCIATION \'S DIRECTOR JUSTIFICATION:</td><td>'+aData[9]+'</td></tr>';
      sOut += '<tr><td>DECLARATION JUSTIFICATION:</td><td>'+aData[10]+'</td></tr>';
      sOut += '</table>';

      return sOut;
    }

    $(document).ready(function() {
      /*
       * Insert a 'details' column to the table
       */
      var nCloneTh = document.createElement('th');
      var nCloneTd = document.createElement('td');
      nCloneTd.innerHTML = '<img src="'+details_open_image+'">';
      nCloneTd.className = "center";

      $('#hidden-table-info thead tr').each(function() {
        this.insertBefore(nCloneTh, this.childNodes[0]);
      });

      $('#hidden-table-info tbody tr').each(function() {
        this.insertBefore(nCloneTd.cloneNode(true), this.childNodes[0]);
      });

      /*
       * Initialse DataTables, with no sorting on the 'details' column
       */
      var oTable = $('#hidden-table-info').dataTable({
        "aoColumnDefs": [{
          "bSortable": false,
          "aTargets": [0]
        }],
        "aaSorting": [
          [1, 'asc']
        ]
      });

      /* Add event listener for opening and closing details
       * Note that the indicator for showing which row is open is not controlled by DataTables,
       * rather it is done here
       */
      $('#hidden-table-info tbody td img').on('click', function() {
        var nTr = $(this).parents('tr')[0];
        if (oTable.fnIsOpen(nTr)) {
          /* This row is already open - close it */
          this.src = details_open_image;
          oTable.fnClose(nTr);
        } else {
          /* Open this row */
          this.src = details_close_image;
          oTable.fnOpen(nTr, fnFormatDetails(oTable, nTr), 'details');
        }
      });
    });
    $.ajaxSetup({
     beforeSend: function(xhr, settings) {
         function getCookie(name) {
             var cookieValue = null;
             if (document.cookie && document.cookie != '') {
                 var cookies = document.cookie.split(';');
                 for (var i = 0; i < cookies.length; i++) {
                     var cookie = jQuery.trim(cookies[i]);
                     // Does this cookie string begin with the name we want?
                     if (cookie.substring(0, name.length + 1) == (name + '=')) {
                         cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                         break;
                     }
                 }
             }
             return cookieValue;
         }
         if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
             // Only send the token to relative URLs i.e. locally.
             xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
         }
     }
});
    $("input[type='checkbox']").change(function() {
    // this will contain a reference to the checkbox
    if (this.checked) {
        // the checkbox is now checked
      $.ajax({

                url: '/user/'+this.name+'/',
                dataType: 'json',
                type: 'post',
                data: {
                  'users_id':this.name,
                  'data':this.id,
                  'value': true
                },
                success: function( data ){
                    console.log('done');
                },
                error: function( data){
                    console.log( data );
                }
            });
    } else {
        // the checkbox is now no longer checked
         $.ajax({

                url: '/user/'+this.name+'/',
                type: 'post',
                dataType: 'json',
                data: {
                  'users_id':this.name,
                  'data':this.id,
                  'value': false
                },
                success: function( data ){
                    console.log('done');
                },
                error: function( data ){
                    console.log( data );
                }
            });

    }
});