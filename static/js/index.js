(function($){
  $.ajaxSetup({
         beforeSend: function(xhr, settings) {
             function getCookie(name) {
                 var cookieValue = null;
                 if (document.cookie && document.cookie !== '') {
                     var cookies = document.cookie.split(';');
                     for (var i = 0; i < cookies.length; i++) {
                         var cookie = jQuery.trim(cookies[i]);
                         // Does this cookie string begin with the name we want?
                         if (cookie.substring(0, name.length + 1) === (name + '=')) {
                             cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                             break;
                         }
                     }
                 }
                 return cookieValue;
             }
             if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                 // Only send the token to relative URLs i.e. locally.
                 xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
             }
         }
        });
      $('#blockSpinner').hide() ; // Hide it initially

$("#blockSearch").keyup(function(e){
    if(e.keyCode == 13) {
      $("#blockSpinner").show();
      $.ajax({
        type: "get",
        dataType: "json",
        data: {"blockID": $("#blockSearch").val()},
        url: "/getblock",
        success: function (data) {
            $("#block-result").empty();
            if (data.web3_connected){
                if (data.error){
                 $("#block-result").html("<div class='alert alert-danger'><b>Error!</b>"+data.error+"</div>");
                }
                let x, txt = "";

                txt += "<table class='table'>";
                txt+="<thread><tr><th>Key</th><th> value </th></tr></thread><tbody>";

                for (x in data.result) {
                  txt += "<tr><td>"+ x +"</td><td>" + data.result[x] + "</td></tr>";
                }
                txt += "</tbody></table>";

                $("#block-result").html(txt);

            }else {
             $("#block-result").html("<div class='alert alert-danger'><b>Error!</b> Web3 is not connected</div>");

            }

            console.log(data);
          $('#blockSpinner').hide(); // Hide it initially

        }
      });
    }

});

})(jQuery);