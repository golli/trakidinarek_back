from django.urls import path

from TrakiDinarek_association import views

urlpatterns = [
    path("association/", views.association, name='association_profile'),  # asscociation management
    path("startproject/", views.startproject, name='startproject'),  # add project
]
