from django import template
from django.utils.safestring import mark_safe

from DataMining.settings import MEDIA_URL
from TrakiDinarek_login.models import User, Association

register = template.Library()


@register.simple_tag
def association_picture(user_association: Association):
    try:
        if not user_association.photo is None and user_association.photo != "":
            return user_association.photo.url
        else:
            return MEDIA_URL + "/Images/AssociationPicture/house.jpg"
    except Exception as e :
        print(e)
        return MEDIA_URL + "/Images/AssociationPicture/house.jpg"


@register.simple_tag
def association_name(user_association: Association):
    try:
        if user_association.name is None or user_association.name == "":
            return "My association "
        else:
            return user_association.name
    except:
        return ''


@register.simple_tag
def association_discription(user_association: Association):
    try:
        if user_association.information is None or user_association.information == "":
            return " "
        else:
            return user_association.information
    except:
        return ''

@register.simple_tag
def association_contact(user_association: Association):

    return mark_safe('<p> Phone: {} <br/>Email :{}<br/> </p> <br><p> Website: {} </p>'.format(user_association.phone,
                                                                                              user_association.email,
                                                                                              user_association.website))


@register.simple_tag
def association_director(user_association: User):
    try:
        return user_association.first_name+' '+user_association.last_name
    except:
        return ''


@register.simple_tag
def association_address(user_association: Association):
    try:
        return mark_safe('Postal address <br/> {}<br/> {}<br/>{}'.format(user_association.address, user_association.city,
                                                                     user_association.zip))
    except:
        return mark_safe(
            'Postal address <br/> {}<br/> {}<br/>{}'.format('', '',
                                                            ''))






@register.simple_tag
def filename(path):
    path_directories = str(path).split("/")
    name = path_directories[-1].split(".")
    return name[0]
