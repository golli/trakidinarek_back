import datetime

from django import forms
from django.forms import TextInput, Textarea, widgets
from django.template.defaultfilters import filesizeformat
from django_summernote.widgets import SummernoteWidget
from nltk.lm.vocabulary import _

from TrakiDinarek_association.formatChecker import RestrictedFileField
from TrakiDinarek_login.forms import CustomModelChoiceField
from TrakiDinarek_login.models import Association, Category

"""'information': SummernoteWidget(
               attrs={'placeholder': 'write a detailed description here', 'required': True}),"""


class AssociationForm(forms.ModelForm):
    class Meta:
        model = Association
        fields = ['name', 'zip', 'city', 'address', 'email', 'information', 'photo', 'website', 'phone','lat','long']
        widgets = {
            'name': TextInput(attrs={'placeholder': "Association's name", 'required': True}),
            'zip': TextInput(attrs={'placeholder': 'Zip code', 'required': True}),
            'city': TextInput(attrs={'placeholder': 'city', 'required': True}),
            'email': TextInput(attrs={'placeholder': 'Email', 'required': True}),
            'phone': TextInput(attrs={'placeholder': 'Phone number ', 'required': True}),
            'address': TextInput(attrs={'placeholder': 'address', 'required': True}),
            'lat': TextInput(attrs={'required': True}),
            'long': TextInput(attrs={'required': True}),
            'information': Textarea(
                attrs={'placeholder': 'Descripe your association key area', 'required': True, 'rows': 5, 'cols': 10,
                       'style': "resize:none"}, ),
            'website': TextInput(attrs={'placeholder': 'http://', 'required': False})

        }

    def __init__(self, *args, **kwargs):
        super(AssociationForm, self).__init__(*args, **kwargs)
        self.fields['photo'].required = False

        try:
            for visible in self.visible_fields():
                if visible.name != "photo":
                    visible.field.widget.attrs['class'] = 'form-control'
        except:
            pass


class AddProject(forms.Form):
    title = forms.CharField(required=True,
                            label='Title',
                            max_length=60,
                            help_text='Give backers the best first impression of your project with great titles.')

    subtitle = forms.CharField(required=True,
                               label='subTitle',
                               max_length=135,
                               help_text='what best describes your project.')
    Project_image = RestrictedFileField(required=True,
                                        help_text='Add an image that clearly represents your project.',
                                        max_upload_size=104857600,
                                        content_types=['image'])
    Project_video = RestrictedFileField(required=False, help_text='Add a video that describes your project.',
                                        max_upload_size=429916160,
                                        content_types=['video'])
    Funding_goal = forms.IntegerField(required=False,
                                      help_text='Make sure your funding goal is practical.',
                                      label='Funding goal',
                                      widget=forms.NumberInput(attrs={'min': 1,
                                                                      'max': 1000000,
                                                                      }))

    Campaign_duration = forms.DateField(initial=datetime.date.today,
                                        required=True,
                                        label='Campaign end date',
                                        help_text='Campaigns that last 30 days or '
                                                  'less are more likely to be '
                                                  'successful.',
                                        widget=forms.DateInput(format='%d/%m/%Y'), input_formats=('%d/%m/%Y',))
    starting_date = forms.DateField(initial=datetime.date.today,
                                    required=True,
                                    label='starting date',
                                    help_text='Campaigns that last 30 days or '
                                              'less are more likely to be '
                                              'successful.',
                                    widget=forms.DateInput(format='%d/%m/%Y'), input_formats=('%d/%m/%Y',))
    CHOICES = [('social_action', 'social action'),
               ('funding', 'funding')]

    Funding_needed = forms.ChoiceField(initial='social_action', label='what do you need ? ', choices=CHOICES,
                                       widget=forms.RadioSelect)
    location = forms.CharField(help_text='Enter the location that best describes where your project is based.',
                               label='Project location',
                               required=True)
    lat = forms.CharField(required=True, widget=forms.TextInput(attrs={'display': 'none'}))
    lon = forms.CharField(required=True, widget=forms.TextInput(attrs={'display': 'none'}))
    Description = forms.CharField(label='Detailed description',
                                  help_text='Describe what you\'re raising funds to do, why you care about it, '
                                            'how you plan to make it happen, and who you are. Your description '
                                            'should tell backers everything they need to know. If possible, include '
                                            'images to show them what your project is all about and what rewards look like.',

                                  widget=SummernoteWidget(attrs={'width': '100%'}))

    favorite_topics = CustomModelChoiceField(required=False,
                                             widget=forms.CheckboxSelectMultiple,
                                             queryset=Category.objects.all())
    bottlenecks = forms.CharField(required=True,
                                  label='Risks and challenges',
                                  widget=forms.Textarea(attrs={'rows': 8,
                                                               'cols': 22,
                                                               'style': 'resize:none;'}),
                                  max_length=255,
                                  help_text='Communicate risks and challenges up '
                                            'front to set proper expectations.')

    def __init__(self, *args, **kwargs):
        super(AddProject, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            if visible.name not in ['Description', 'Project_video', 'Project_image', 'Funding_needed']:
                visible.field.widget.attrs['class'] = 'form-control'
            if visible.name == 'Description':
                visible.field.widget.attrs['class'] = 'summernote'
