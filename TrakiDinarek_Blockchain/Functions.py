import string

from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK
from rest_framework.views import APIView
from web3 import Web3, HTTPProvider

from DataMining import settings
from TrakiDinarek_Blockchain.contracts import TDCoin_ABI
from TrakiDinarek_Blockchain.models import BuyTokensTransactions, BlockchainAccount
from TrakiDinarek_login.models import User
from Utility.Fernet import encryption_util


def transfer(to_address: string, amount: int, type: string):
    web3 = Web3(HTTPProvider(settings.Blockchain_IP_address))
    if web3.isConnected():
        sender = settings.ADMIN_ACCOUNT
        account = BlockchainAccount.objects.get(blockchain_address=to_address)
        receiver = account.user
        td_coin_contract = web3.eth.contract(address=settings.TDCoin_CONTRACT_address, abi=TDCoin_ABI.abi)
        func_to_call = 'transfer'
        transfer_method = td_coin_contract.functions[func_to_call]
        tx_hash = transfer_method(to_address,
                                  int(amount)).transact({'from': sender})
        tx_receipt = web3.eth.waitForTransactionReceipt(tx_hash)
        if tx_receipt is None:
            transfer(to_address=to_address, amount=amount, type=type)
        logs = td_coin_contract.events.Transfer().processReceipt(tx_receipt)[0]['args']
        if type == 'buy':
            new_transaction = BuyTokensTransactions.objects.create(transactionHash=web3.toHex(tx_hash),
                                                                   sender="Admin",
                                                                   receiver=receiver,
                                                                   amount=amount)
        print(web3.eth.getTransaction(tx_hash))
    else:
        return {'status': False, 'error': 'Could not connect to Blockchain server'}


def approve(user: User, project_address: string, amount: int):
    web3 = Web3(HTTPProvider(settings.Blockchain_IP_address, request_kwargs={'timeout': 60}))
    if web3.isConnected():
        td_coin_contract = web3.eth.contract(address=settings.TDCoin_CONTRACT_address,
                                             abi=TDCoin_ABI.abi)
        # approve contract to use Donors money
        func_to_call = 'approve'
        approve = td_coin_contract.functions[func_to_call]
        sender_address = user.blockchain.blockchain_address
        if amount <= td_coin_contract.functions.balanceOf(user).call():
            sender_password = (encryption_util.decrypt(user.blockchain.blockchain_password)).replace(
                user.email + ' ', '')
            if web3.geth.personal.unlockAccount(sender_address, sender_password):
                transfer_hash = approve(project_address, amount).transact({'from': sender_address, 'gas': '300000'})
                transfer_receipt = web3.eth.waitForTransactionReceipt(transfer_hash)
                logs = td_coin_contract.events.Approval().processReceipt(transfer_receipt)
                if transfer_receipt is None:
                    approve(user=user, project_address=string, amount=amount)
                logs = td_coin_contract.events.Transfer().processReceipt(transfer_receipt)[0]['args']
                if logs:
                    return {'status': True, 'message': 'Approval succeded'}
    else:
        return {'status': False, 'error': 'Could not connect to Blockchain server'}


