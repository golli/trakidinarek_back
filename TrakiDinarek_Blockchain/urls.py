from django.urls import path, re_path

from TrakiDinarek_Blockchain import views
from TrakiDinarek_Blockchain import API


urlpatterns = [
    path("create_blockChainuser/", views.create),  # user account create
    path("balance", views.BalanceOF),  # user checking balance
    path("transactions", views.transactionHistory),  # check transactions
    path("new_project", views.newProject),  # create new project
    path("project_creation_transactions", views.Project_Creation_History),  # user project creation history
    path("donate", views.donate),  # donate to project
    path("ProjectDetails",views.ProjectDetails),
    path("ProjectDonations", views.Project_Donations_history),
    path("allowance", views.allowance),
    path("status/", API.NodeStatus),
    re_path("getblock/$", API.getblock),
    re_path("gettransaction/$", API.getTransaction)

]
