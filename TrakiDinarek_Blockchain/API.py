from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK
from web3 import Web3, HTTPProvider
from datetime import datetime
import pytz

from DataMining import settings


@api_view(('GET',))
@permission_classes(())
def NodeStatus(request):
    web3 = Web3(HTTPProvider(settings.Blockchain_IP_address))
    if web3.isConnected():

        return Response({'web3 connected': True,
                         'status': web3.eth.syncing,
                         'latest block': web3.eth.blockNumber,

                         }, status=HTTP_200_OK)
    else:
        return Response({'web3 connected': False,
                         }, status=HTTP_200_OK)


@api_view(('GET',))
@permission_classes(())
def getblock(request):
    web3 = Web3(HTTPProvider(settings.Blockchain_IP_address))
    if web3.isConnected() and request.GET['blockID']:

        local_tz = pytz.timezone("Africa/Tunis")

        try:
            block = web3.eth.getBlock(int(request.GET['blockID']))
            utc_dt = datetime.utcfromtimestamp(block['timestamp']).replace(tzinfo=pytz.utc)
            timestamp = local_tz.normalize(utc_dt.astimezone(local_tz))
            transaction = []
            for tr in block['transactions']:
                transaction.append(Web3.toJSON(tr))
            return Response({'web3_connected': True,
                             'result': {
                                 'hash': Web3.toJSON(block['hash']),
                                 'miner': block['miner'],
                                 'transactions': transaction,
                                 'difficulty': block['difficulty'],
                                 'size': block['size'],
                                 'nonce': Web3.toInt(block['nonce']),
                                 'timestamp': local_tz.normalize(utc_dt.astimezone(local_tz))
                             }
                             }, status=HTTP_200_OK)
        except ValueError:
            block = web3.eth.getBlock(request.GET['blockID'])
            utc_dt = datetime.utcfromtimestamp(block['timestamp']).replace(tzinfo=pytz.utc)
            timestamp = local_tz.normalize(utc_dt.astimezone(local_tz))
            transaction = []
            for tr in block['transactions']:
                transaction.append(Web3.toJSON(tr))
            return Response({'web3_connected': True,
                             'result': {
                                 'hash': Web3.toJSON(block['hash']),
                                 'miner': block['miner'],
                                 'transactions': transaction,
                                 'difficulty': block['difficulty'],
                                 'size': block['size'],
                                 'nonce': Web3.toInt(block['nonce']),
                                 'timestamp': local_tz.normalize(utc_dt.astimezone(local_tz))}

                             }, status=HTTP_200_OK)
        except Exception as e:
            return Response({'web3 connected': False,
                             'error': e.args[0]
                             }, status=HTTP_200_OK)
    else:
        return Response({'web3 connected': False,
                         }, status=HTTP_200_OK)


@api_view(('GET',))
@permission_classes(())
def getTransaction(request):
    web3 = Web3(HTTPProvider(settings.Blockchain_IP_address))
    if web3.isConnected():

        return Response({'web3 connected': True,
                         'transaction_details': web3.eth.getTransaction('hash'),
                         'transaction_receipt': web3.eth.getTransactionReceipt('hash')

                         }, status=HTTP_200_OK)
    else:
        return Response({'web3 connected': False,
                         }, status=HTTP_200_OK)
