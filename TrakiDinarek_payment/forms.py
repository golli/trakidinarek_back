from django import forms


class CartForm(forms.Form):
    Amount = forms.IntegerField(required=False)
    Amount_to_add = forms.IntegerField(required=True, min_value=5000, max_value=1000000,label='Amount to add')
