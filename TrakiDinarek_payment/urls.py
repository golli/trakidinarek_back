from django.urls import path

from TrakiDinarek_payment import views

urlpatterns = [
    path("cart_details/", views.cart_details, name='cart_details'),  # make_order
    path("order/", views.process_payment, name='order_cart'),  # place_order
    path("done/", views.payment_done, name='payment_done'),
    path("order_invoice/<str:uidb64>/<str:token>/<str:order>", views.order_invoice, name='order_invoice'),

]
