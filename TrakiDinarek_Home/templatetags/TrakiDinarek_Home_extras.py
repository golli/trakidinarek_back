from django import template

from DataMining import settings
from TrakiDinarek_Blockchain.contracts import Project_sol, ProjectFactory_sol, TDCoin_ABI
from TrakiDinarek_login.models import User
from Utility.Fernet import encryption_util

register = template.Library()


@register.simple_tag
def blockchain_server():
    return settings.Blockchain_IP_address


@register.simple_tag
def get_user_blockchain_address(user: User):
    try:
        return str(user.blockchain.blockchain_address)

    except:
        return None

@register.simple_tag
def get_user_blockchain_password(user: User):
    try:
        return (encryption_util.decrypt(user.blockchain.blockchain_password)).replace(user.email + ' ', '')
    except:
        return None

@register.simple_tag
def getProjectABI():
    return Project_sol.abi


@register.simple_tag
def getProjectAddress(id):
    return "0x40ADdbE711f2adB6d4A3Adc14F182762a22ec2d2"


@register.simple_tag
def getTokenABI():
    return TDCoin_ABI.abi


@register.simple_tag
def getTokenAddress():
    return settings.TDCoin_CONTRACT_address


@register.simple_tag
def get_Admin_blockchain_address():
    return settings.ADMIN_ACCOUNT
