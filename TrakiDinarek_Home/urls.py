from django.urls import path

from TrakiDinarek_Home import views

urlpatterns = [
    path("", views.Home, name='home'),  # home
    path("causes/", views.causes, name='causes'),  # causes
    path("about_us/", views.about, name='about'),  # about
    path("gallery/", views.gallery, name='gallery'),  # gallery
    path("news/", views.news, name='news'),  # news
    path("contact/", views.contact, name='contact'),  # contact
    path("single_causes/", views.singleCauses, name='signle'),  # contact
    path("element/", views.element, name='element'),  # contact

]
