import smtplib

import facebook
import jwt
from django.contrib import messages
from django.contrib.auth import user_logged_in, login, authenticate
from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import EmailMessage
from django.db import transaction
from django.http import JsonResponse, Http404, HttpResponsePermanentRedirect
from django.shortcuts import render, redirect
# Create your views here.
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.views.decorators.csrf import ensure_csrf_cookie
from rest_framework.decorators import api_view, permission_classes
from rest_framework_jwt.utils import jwt_payload_handler

from DataMining import settings
from Utility.Celery_Tasks.Mailer import Mailer
from TrakiDinarek_login.forms import SignUpForm, LoginForm, SendEmailForm, ResetPasswordForm, ActivateForm
from TrakiDinarek_login.tokens import account_password_reset_generator, account_activation_token
from .models import User, FacebookAccount, UserProfile


@ensure_csrf_cookie
@api_view(('POST', 'GET'), )
@permission_classes(())
def login_user(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']
            remember_me = form.cleaned_data['remember_me']
            user = authenticate(request, email=email, password=password)
            if not user.is_active:
                mail = Mailer()

                # Send an email to the user with the token:
                current_site = get_current_site(request)
                uid = urlsafe_base64_encode(force_bytes(user.pk)).decode()
                token = account_activation_token.make_token(user)
                activation_link = "{0}/uid={1}&token={2}".format(current_site, uid, token)
                message = "Hello {0},\n {1}".format(user.first_name + " " + user.last_name, activation_link)
                to_email = [user.email]
                # email = EmailMessage(mail_subject, message, to=[to_email])
                # email.send()
                mail.send_messages(subject='Activate your account.',
                                   template='TrakiDinarek_login/acc_active_email.html',
                                   context={
                                       'user': user,
                                       'domain': current_site.domain,
                                       'uid': urlsafe_base64_encode(force_bytes(user.pk)).decode(),
                                       'token': token,
                                   },
                                   to_emails=to_email)
                messages.success(request, 'Check your email for activation link')
                return render(request, 'activate.html')
            else:
                login(request, user)
                if not remember_me:
                    request.session.set_expiry(0)
                return HttpResponsePermanentRedirect(reverse('homepage'))
        else:

            return render(request, 'login.html', {'form': form, 'logout': False})

    else:
        user = request.user
        if user.is_anonymous:
            form = LoginForm()
            return render(request, 'login.html', {'form': form, 'logout': False})
        else:
            return HttpResponsePermanentRedirect(reverse('homepage'))


@api_view(('POST',))
@permission_classes(())
def authenticate_facebook(request):
    data = request.POST
    if data['userID']:
        graph = facebook.GraphAPI(data['accessToken'])
        fields = ['last_name,first_name,email']
        profile = graph.get_object('me', fields=fields)
        picture = graph.request(data['userID'] + '/picture?height=300')

        try:
            """Facebook user exists"""
            user = User.objects.get(facebook=profile['id'])

            if user.facebook.fb_token != data['accessToken']:
                user.facebook.fb_token = data['accessToken']
            user.facebook.image_url=picture['url']
            user.facebook.save()
            user_logged_in.send(sender=user.__class__,
                                request=request, user=user)

            login(request, user)
            data = {
                'success': 1,
                'url': reverse('homepage')
            }
            return JsonResponse(data)

        except User.DoesNotExist:
            """Facebook user doesn't exist"""
            password = User.objects.make_random_password()
            user = User(email=profile['email'], first_name=profile['first_name'],
                        last_name=profile['last_name'], is_active=True
                        )
            user.set_password(password)
            user.save()
            login(request, user)
            facebook_info = FacebookAccount.objects.create(first_name=profile['first_name'],
                                                           image_url=picture['url'],
                                                           last_name=profile['last_name'],
                                                           fb_id=profile['id'],
                                                           fb_token=data['accessToken'],
                                                           user=user)

            data = {
                'success': 1,
                'url': reverse('signup')
            }
            return JsonResponse(data)
        except UserProfile.DoesNotExist:

            return JsonResponse({
                'success': True,
                'url': reverse('signup', args=[]),
            })
        except Exception as e:
            print(e)


def redirect_view(request, pk):
    response = redirect(pk)
    return response


@transaction.atomic
@api_view(('POST', 'GET',))
@permission_classes(())
def signup(request):
    user = request.user
    if not user.is_anonymous:  # signup with Facebook
        if request.method == 'POST':
            # POST, generate form with data from the request
            form = SignUpForm(request.POST, request.FILES)

            if form.is_valid():
                zip = form.cleaned_data['zip']
                address = form.cleaned_data['address']
                city = form.cleaned_data['city']
                picture = form.cleaned_data['photo']
                phone = form.cleaned_data['phone_number']
                first_name = form.cleaned_data['first_name']
                last_name = form.cleaned_data['last_name']
                categories = form.cleaned_data['favorite_topics']
                user_profile, created = UserProfile.objects.get_or_create(user=user)
                user_profile.zip = zip
                user_profile.address = address
                user_profile.city = city
                user_profile.phone = phone
                user_profile.photo = picture
                user_profile.save()
                user.first_name = first_name
                user.last_name = last_name
                user.save()
                return HttpResponsePermanentRedirect(reverse('homepage'))

            else:
                form.fields['email'].widget.attrs['readonly'] = True  # text input

                return render(request,
                              'Signup.html',
                              {'form': form, 'facebook_account': True})
        else:
            try:
                # GET, generate blank form
                facebook_info = FacebookAccount.objects.get(user=user)
                """results = preferences(user)
                try:
                    if "error" in results:
                        del request.session['user_id']
                        form = LoginForm
                        return render(request, 'login.html', {'form': form, 'logout': True})
                except Exception as e:
                    print(e)
                
"""
                if facebook_info.image_url is None:
                    im = "/Images/UsersPictures/default-profile.jpeg"
                    form = SignUpForm(
                        initial={'first_name': facebook_info.first_name, 'last_name': facebook_info.last_name,
                                 'email': user.email, 'photo': im})
                else:
                    form = SignUpForm(
                        initial={'first_name': facebook_info.first_name, 'last_name': facebook_info.last_name,
                                 'email': user.email, 'photo': facebook_info.image_url})

                form.fields['email'].widget.attrs['readonly'] = True  # text input

                return render(request, 'Signup.html',
                              {'form': form, 'facebook_account': True})
            except FacebookAccount.DoesNotExist:
                pass
    else:
        # new user without FB
        if request.method == 'POST':
            # POST, generate form with data from the request
            form = SignUpForm(request.POST)
            if form.is_valid():
                try:
                    with transaction.atomic():
                        zip = form.cleaned_data['zip']
                        address = form.cleaned_data['address']
                        city = form.cleaned_data['city']
                        email = form.cleaned_data['email']
                        first_name = form.cleaned_data['first_name']
                        last_name = form.cleaned_data['last_name']
                        password = form.cleaned_data['password']
                        picture = form.cleaned_data['photo']
                        phone = form.cleaned_data['phone_number']
                        first_name = form.cleaned_data['first_name']
                        last_name = form.cleaned_data['last_name']
                        categories = form.cleaned_data['favorite_topics']
                        user = User(email=email, first_name=first_name,
                                    last_name=last_name, is_active=False
                                    )
                        user.save()
                        user.set_password(password)
                        user_profile, created = UserProfile.objects.get_or_create(user=user)
                        user_profile.zip = zip
                        user_profile.address = address
                        user_profile.city = city
                        user_profile.phone = phone
                        user_profile.photo = picture
                        user_profile.save()
                        user.first_name = first_name
                        user.last_name = last_name
                        user.save()
                        mail = Mailer()

                        # Send an email to the user with the token:
                        current_site = get_current_site(request)
                        uid = urlsafe_base64_encode(force_bytes(user.pk)).decode()
                        token = account_activation_token.make_token(user)
                        activation_link = "{0}/uid={1}&token={2}".format(current_site, uid, token)
                        message = "Hello {0},\n {1}".format(user.first_name + " " + user.last_name, activation_link)
                        to_email = [user.email]
                        # email = EmailMessage(mail_subject, message, to=[to_email])
                        # email.send()
                        mail.send_messages(subject='Activate your account.',
                                           template='TrakiDinarek_login/acc_active_email.html',
                                           context={
                                               'user': user,
                                               'domain': current_site.domain,
                                               'uid': urlsafe_base64_encode(force_bytes(user.pk)).decode(),
                                               'token': account_password_reset_generator.make_token(user),
                                           },
                                           to_emails=to_email)
                        messages.success(request, 'Check your email for activation link')
                        return render(request, 'activate.html')
                except Exception as ex:
                    return render(request, 'Signup.html',
                                  {'form': form, 'facebook_account': False})

            else:
                return render(request, 'Signup.html',
                              {'form': form, 'facebook_account': False})
        else:
            form = SignUpForm()
            form.fields['password'].widget.attrs['required'] = True
            form.fields['confirm_password'].widget.attrs['required'] = True
            im = "/Images/UsersPictures/default-profile.jpeg"
            form = SignUpForm(
                initial={'photo': im})
            return render(request, 'Signup.html',
                          {'form': form, 'facebook_account': False})


@api_view(('POST', 'GET',))
@permission_classes(())
def send_reset_password(request):
    if request.method == 'POST':
        form = SendEmailForm(request.POST)
        if form.is_valid():
            mail_subject = 'Password Reset.'
            current_site = get_current_site(request)
            user = User.objects.get(email=form.cleaned_data['email'])
            message = render_to_string('TrakiDinarek_login/acc_reset_password.html', {
                'user': user,
                'domain': current_site.domain,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)).decode(),
                'token': account_password_reset_generator.make_token(user),
            })
            to_email = user.email
            email = EmailMessage(mail_subject, message, to=[to_email])
            try:
                email.send(fail_silently=False)
                messages.success(request, 'Please check your email to reset your password.')

            except smtplib.SMTPException:
                raise form.ValidationError("we couldn't send your reset password mail, please try again")
            except Exception as e:
                print(e)

            return render(request, 'account.html', {'form': form})

        else:
            return render(request, 'account.html', {'form': form})

    else:
        form = SendEmailForm()
        return render(request, 'account.html', {'form': form})


@api_view(('POST', 'GET',))
@permission_classes(())
def reset_password(request, uidb64, token):
    if request.method == 'POST':
        form = ResetPasswordForm(request.POST)
        if form.is_valid():
            uid = urlsafe_base64_decode(uidb64).decode()
            user = User.objects.get(pk=uid)
            user.set_password(form.cleaned_data['password'])
            user.save()
            messages.success(request, 'password has successfully been changed')
            return render(request, 'account.html', {'form': form})
        else:
            return render(request, 'account.html', {'form': form})

    else:
        try:
            uid = urlsafe_base64_decode(uidb64).decode()
            user = User.objects.get(pk=uid)
        except(TypeError, ValueError, OverflowError, User.DoesNotExist):
            user = None
        if user is not None and account_password_reset_generator.check_token(user, token):
            form = ResetPasswordForm()
            return render(request, 'account.html', {'form': form})

        else:
            raise Http404


@api_view(('GET','POST'))
@permission_classes(())
def Activate(request, uidb64, token):
    if request.method == 'POST':
        form = ActivateForm(request.POST)
        if form.is_valid():
            user = User.objects.get(email=form.cleaned_data['email'])
            mail = Mailer()

            # Send an email to the user with the token:
            current_site = get_current_site(request)
            uid = urlsafe_base64_encode(force_bytes(user.pk)).decode()
            token = account_activation_token.make_token(user)
            to_email = [user.email]
            mail.send_messages(subject='Activate your account.',
                               template='TrakiDinarek_login/acc_active_email.html',
                               context={
                                   'user': user,
                                   'domain': current_site.domain,
                                   'uid': uid,
                                   'token': token,
                               },
                               to_emails=to_email)
            messages.success(request, 'Check your email for activation link')
            return render(request, 'activate.html')
    else:
        try:
            uid = urlsafe_base64_decode(uidb64).decode()
            user = User.objects.get(pk=uid)
        except(TypeError, ValueError, OverflowError, User.DoesNotExist):
            user = None
        if user is not None and account_activation_token.check_token(user, token):
            user.is_active = True
            user.save()
            payload = jwt_payload_handler(user)
            token = jwt.encode(payload, settings.SECRET_KEY)
            messages.success(request, 'Thank you for your email confirmation. Now you can login your account.')
            return render(request, 'activate.html')
        else:
            form = ActivateForm()
            messages.success(request, 'Activation link is invalid!')
            return render(request, 'activate.html', {'form': form})
