from django.urls import path

from TrakiDinarek_login import views

urlpatterns = [

    path("login/", views.login_user, name="login"),  # Home page (login)
    path("registration/", views.signup, name="signup"),
    path("authenticateFacebook/", views.authenticate_facebook),
    path("activate/<str:uidb64>/<str:token>/", views.Activate, name='activate'),
    path("resetPassword/<str:uidb64>/<str:token>/", views.reset_password, name='resetPassword'),
    path("resetPassword/", views.send_reset_password, name="send_reset_password")

]
