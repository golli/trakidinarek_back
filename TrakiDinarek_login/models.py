import os
from io import StringIO

from PIL import Image
from django.contrib.auth.models import AbstractUser
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.db import models
# Create your models here.
from django.utils.translation import ugettext_lazy as _
from django_cleanup import cleanup
from phonenumber_field.modelfields import PhoneNumberField

from DataMining import settings


class EventCategory(models.Model):
    category = models.ForeignKey('TrakiDinarek_login.Category', on_delete=models.CASCADE)
    event = models.ForeignKey('TrakiDinarek_login.Event', on_delete=models.CASCADE)


class Category(models.Model):
    name = models.CharField(max_length=255)
    image = models.ImageField(upload_to='Images/', blank=True)
    desc = models.CharField(max_length=255)

    def save(self, *args, **kwargs):
        instance = super(Category, self).save(*args, **kwargs)
        image = Image.open(instance.photo.path)
        image.save(instance.photo.path, quality=60, optimize=True)
        return instance

    def __str__(self):
        return f'{self.name} ({self.image})'


class Event(models.Model):
    association = models.ForeignKey('Association', on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    goal = models.TextField()
    target = models.CharField(max_length=255)
    address = models.CharField(max_length=255)
    categories = models.ManyToManyField(Category, through='EventCategory')
    closing_date = models.DateField()
    timestamp_last_updated = models.DateTimeField(auto_now=True)
    timestamp_added = models.DateTimeField(auto_now_add=True)


class FacebookAccount(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE,
                                related_name='facebook')
    fb_id = models.CharField(max_length=255, blank=True, primary_key=True)
    fb_token = models.TextField(max_length=255, blank=True)
    first_name = models.CharField(max_length=255, blank=True, null=True)
    last_name = models.CharField(max_length=255, blank=True, null=True)
    image_url = models.URLField(blank=True, null=True)


class User(AbstractUser):
    username = models.CharField(max_length=255, blank=True, null=True)
    email = models.EmailField(_('email address'), unique=True, default=None)
    USERNAME_FIELD = 'email'
    password = models.CharField(_('password'), max_length=128, default=None, null=True)

    REQUIRED_FIELDS = ['username', 'first_name', 'last_name']
    balance = models.CharField(max_length=255, null=False, default="0")

    is_active = models.BooleanField(
        _('active'),
        default=False,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )
    interests = models.ManyToManyField(Category, through='CategoryUser', blank=True)
    favorite = models.ManyToManyField(Event, through='EventUserFavorite', related_name='favorites',
                                      blank=True)
    donations = models.ManyToManyField(Event, through='Donor', related_name='donations', blank=True)


class UserProfile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE,
                                related_name='profile')
    address = models.CharField(max_length=30, null=True, blank=True)
    city = models.CharField(max_length=30, null=True, blank=True)
    phone = PhoneNumberField(null=True, blank=True, unique=True)
    photo = models.ImageField(upload_to='Images/UsersPictures', null=True, blank=True, verbose_name=_("UserProfile"))
    zip = models.CharField(max_length=8, null=True, blank=True)
    timestamp_last_updated = models.DateTimeField(auto_now=True)
    timestamp_added = models.DateTimeField(auto_now_add=True)

    def save(self, *args, **kwargs):
        try:
            if self.photo:
                img = Image.open(StringIO.StringIO(self.image.read()))
                if img.mode != 'RGB':
                    img = img.convert('RGB')
                img.thumbnail((self.image.width / 1.5, self.image.height / 1.5), Image.ANTIALIAS)
                output = StringIO.StringIO()
                img.save(output, format='JPEG', quality=70)
                output.seek(0)
                self.image = InMemoryUploadedFile(output, 'ImageField', "%s.jpg" % self.image.name.split('.')[0],
                                                  'image/jpeg', output.len, None)
        except Exception as e:
            print(e)
        super(UserProfile, self).save(*args, **kwargs)


class EventUserFavorite(models.Model):
    user = models.ForeignKey('TrakiDinarek_login.User', on_delete=models.CASCADE)
    event = models.ForeignKey('TrakiDinarek_login.Event', on_delete=models.CASCADE)
    timestamp_added = models.DateTimeField(auto_now_add=True)


class Donor(models.Model):
    user = models.ForeignKey('TrakiDinarek_login.User', on_delete=models.CASCADE)
    event = models.ForeignKey('TrakiDinarek_login.Event', on_delete=models.CASCADE)
    timestamp_added = models.DateTimeField(auto_now_add=True)


def upload_JORT_handler(instance, filename):
    return "users_documents/user_{name}/JORT/{file}".format(name=instance.user.get_full_name(), file=filename)


def upload_presidentJust_handler(instance, filename):
    return "users_documents/user_{name}/presidentJust/{file}".format(name=instance.user.get_full_name(), file=filename)


def upload_declarations_handler(instance, filename):
    return "users_documents/user_{name}/declarations/{file}".format(name=instance.user.get_full_name(), file=filename)


@cleanup.ignore
class UserDocuments(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, blank=True, default=None,
                                related_name='documents')
    jort_justification = models.FileField(upload_to=upload_JORT_handler, blank=True)
    president_justification = models.FileField(upload_to=upload_presidentJust_handler, blank=True)
    declaration_justification = models.FileField(upload_to=upload_declarations_handler, blank=True)
    jort_validated = models.BooleanField(default=False, blank=False)
    president_validated = models.BooleanField(default=False, blank=False)
    declaration_validated = models.BooleanField(default=False, blank=False)


class Association(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE,
                                related_name='Association')
    name = models.CharField(max_length=255)
    address = models.CharField(max_length=30, null=True, blank=True)
    email = models.EmailField(_('email address'), null=True, blank=True)
    zip = models.CharField(max_length=8, null=True, blank=True)
    city = models.CharField(max_length=30, null=True, blank=True)
    phone = PhoneNumberField(null=True, blank=True, unique=True)
    information = models.TextField(null=True, blank=True)
    website = models.CharField(max_length=30, null=True, blank=True)
    photo = models.ImageField(upload_to='Images/AssociationPicture', null=True,
                              blank=True, verbose_name=_("UserProfile"))
    lat = models.CharField(max_length=30, null=True, blank=True)
    long = models.CharField(max_length=30, null=True, blank=True)

    def save(self, *args, **kwargs):
        try:
            if self.photo:
                img = Image.open(StringIO.StringIO(self.image.read()))
                if img.mode != 'RGB':
                    img = img.convert('RGB')
                img.thumbnail((self.image.width / 1.5, self.image.height / 1.5), Image.ANTIALIAS)
                output = StringIO.StringIO()
                img.save(output, format='JPEG', quality=70)
                output.seek(0)
                self.image = InMemoryUploadedFile(output, 'ImageField', "%s.jpg" % self.image.name.split('.')[0],
                                                  'image/jpeg', output.len, None)
        except Exception as e:
            print(e)
        super(Association, self).save(*args, **kwargs)


class Payment(Donor):
    Value = models.FloatField(blank=False, null=False)


class Cash(Payment):
    verified = models.BooleanField(default=False)
    Image = models.CharField(max_length=255)


class Online(Payment):
    TransactionId = models.CharField(max_length=255, unique=True)


class Item(Donor):
    Image = models.CharField(max_length=255, blank=False, null=False)
    Description = models.TextField(blank=False, null=False)


class CategoryUser(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    percentage = models.FloatField(default=0)
    selected = models.BooleanField(default=False)

    class Meta:
        ordering = ['-percentage']
        unique_together = (("user", "category"),)


class Pictures(models.Model):
    image_name = models.ImageField(upload_to='Justifications/', blank=True)
    Event = models.ForeignKey(Event, on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        instance = super(Pictures, self).save(*args, **kwargs)
        image = Image.open(instance.photo.path)
        image.save(instance.photo.path, quality=60, optimize=True)
        return instance
