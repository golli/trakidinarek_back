# Generated by Django 2.1.7 on 2019-06-19 15:44

""""""
import os
from sys import path
from django.core import serializers
from django.core.management import call_command
from django.db import migrations

fixture_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), '../fixtures'))
fixture_filename = 'initial_data.json'


def load_fixture(apps, schema_editor):
    fixture_file = os.path.join(fixture_dir, fixture_filename)
    call_command('loaddata', fixture_file)


def unload_fixture(apps, schema_editor):
    # Brutally deleting all entries for this model

    MyModel = apps.get_model("TrakiDinarek_login", "Category")
    MyModel.objects.all().delete()


class Migration(migrations.Migration):
    dependencies = [
        ('TrakiDinarek_login', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(load_fixture, reverse_code=unload_fixture),
    ]
