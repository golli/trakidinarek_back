import jwt
from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.views.decorators.csrf import csrf_exempt
from rest_framework import serializers

from rest_framework_jwt.serializers import jwt_payload_handler

from DataMining import settings
from .models import User, UserProfile
from .tokens import account_activation_token


class UserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        fields = ('fb_id', 'fb_token', 'address', 'city', 'phone', 'photo', 'zip')
        extra_kwargs = {'fb_id': {'write_only': True},
                        'fb_token': {'write_only': True}
                        }


class CreateUserSerializer(serializers.HyperlinkedModelSerializer):
    profile = UserProfileSerializer(required=True)

    # activation_link = serializers.SerializerMethodField('generate_token')

    # def generate_token(self, obj):
    #   user = User.objects.get(email=obj['email'])

    # Send an email to the user with the token:
    #  mail_subject = 'Activate your account.'
    # current_site = get_current_site(self.context['request'])
    # uid = urlsafe_base64_encode(force_bytes(user.pk)).decode()
    # token = account_activation_token.make_token(user)
    # activation_link = "{0}/uid={1}&token={2}".format(current_site, uid, token)
    # message = "Hello {0},\n {1}".format(user.first_name + " " + user.last_name, activation_link)
    # to_email = user.email
    # email = EmailMessage(mail_subject, message, to=[to_email])
    # email.send()
    # return activation_link

    class Meta:
        model = User
        fields = ('email', 'first_name', 'last_name', 'password', 'profile',)
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        profile_data = validated_data.pop('profile')
        password = validated_data.pop('password')
        user = User(**validated_data)
        user.set_password(password)
        user.save()
        UserProfile.objects.create(user=user, **profile_data)
        if user:
            mail_subject = 'Activate your account.'
            current_site = get_current_site(self.context['request'])
            message = render_to_string('TrakiDinarek_login/acc_active_email.html', {
                'user': user,
                'domain': current_site.domain,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)).decode(),
                'token': account_activation_token.make_token(user),
            })
            to_email = user.email
            email = EmailMessage(mail_subject, message, to=[to_email])
            email.send()
            user_details = {'first_name': user.first_name,
                            'last_name': user.last_name,
                            'email': user.email,
                            'profile': UserProfile.objects.get(user=user),
                            }

            # preferences(user)
            return user_details


class UserSerializer(serializers.HyperlinkedModelSerializer):
    profile = UserProfileSerializer(required=True)

    class Meta:
        model = User
        fields = ('email', 'first_name', 'last_name', 'profile')
        extra_kwargs = {'password': {'write_only': True}}

    @csrf_exempt
    def update(self, instance, validated_data):
        profile_data = validated_data.pop('profile')
        profile = instance.profile
        profile.fb_id = profile_data.get('fb_id', profile.fb_id)
        profile.fb_token = profile_data.get('fb_token', profile.fb_token)
        profile.phone = profile_data.get('phone', profile.phone)
        profile.address = profile_data.get('address', profile.address)
        profile.city = profile_data.get('city', profile.city)
        profile.zip = profile_data.get('zip', profile.zip)
        profile.photo = profile_data.get('photo', profile.photo)
        profile.save()
        return instance



        """
        class AssociationSerializer(serializers.ModelSerializer):
    class Meta:
        model = AssociationDirector
        fields = ('id_association',
                  'jort_justification',
                  'president_justification',
                  'declaration_justification')
   def update(self, instance, validated_data):
        profile_data = validated_data.pop('profile')
        profile = instance.profile
        profile.fb_id = profile_data.get('fb_id', profile.fb_id)
        profile.fb_token = profile_data.get('fb_token', profile.fb_token)
        profile.phone = profile_data.get('phone', profile.phone)
        profile.address = profile_data.get('address', profile.address)
        profile.city = profile_data.get('city', profile.city)
        profile.zip = profile_data.get('zip', profile.zip)
        profile.photo = profile_data.get('photo', profile.photo)
        profile.save()
        return instance
        """