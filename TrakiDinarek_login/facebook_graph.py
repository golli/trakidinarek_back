from multiprocessing.pool import ThreadPool

import regex
from django.core.exceptions import ObjectDoesNotExist

from TrakiDinarek_login.models import Category, CategoryUser
import facebook
import requests
import gensim
import numpy as np
import re
import spacy

from gensim.models import LdaModel
from gensim.corpora import Dictionary

health = [
    'Medical & Health',
    'Dentist & Dental Office',
    'Cosmetic Dentist',
    'Endodontist',
    'Sports Promoter',
    'Athlete',
    'General Dentist',
    'Oral Surgeon',
    'Orthodontist',
    'Pediatric Dentist',
    'Periodontist',
    'Prosthodontist',
    'Teeth Whitening Service',
    'Doctor',
    'Hospitality Service',
    'Allergist',
    'Anesthesiologist',
    'Audiologist',
    'Cardiologist',
    'Dermatologist',
    'Endocrinologist',
    'Family Doctor',
    'Fertility Doctor',
    'Gastroenterologist',
    'Gerontologist',
    'Internist (Internal Medicine)',
    'Lasik/Laser Eye Surgeon',
    'Nephrologist',
    'Neurologist',
    'Neurosurgeon',
    'Obstetrician-Gynecologist (OBGYN)',
    'Oncologist',
    'Ophthalmologist',
    'Optometrist',
    'Orthopedist',
    'Osteopathic Doctor',
    'Otolaryngologist (ENT)',
    'Pediatrician',
    'Plastic Surgeon',
    'Podiatrist',
    'Proctologist',
    'Psychiatrist',
    'Psychologist',
    'Pulmonologist',
    'Radiologist',
    'Rheumatologist',
    'Surgeon',
    'Urologist',
    'Medical Center',
    'AIDS Resource Center',
    'Blood Bank',
    'Crisis Prevention Center',
    'Diagnostic Center',
    'Dialysis Clinic',
    'Emergency Room',
    'Family Medicine Practice',
    'Halfway House',
    'Hospice',
    'Hospital',
    'Maternity Clinic',
    'Medical Lab',
    'Nursing Home',
    'Pregnancy Care Center',
    'Retirement & Assisted Living Facility',
    'STD Testing Center',
    'Surgical Center',
    'Women\'s Health Clinic',
    'Medical Device Company',
    'Medical Equipment Manufacturer',
    'Medical Equipment Supplier',
    'Medical Research Center',
    'Medical Service',
    'Abortion Service',
    'Addiction Service',
    'Addiction Resources Center',
    'Addiction Treatment Center',
    'Alcohol Addiction Treatment Center',
    'Drug Addiction Treatment Center',
    'Tobacco Cessation Treatment Center',
    'Alternative & Holistic Health Service',
    'Acupuncturist',
    'Chiropractor',
    'Massage Therapist',
    'Medical Cannabis Dispensary',
    'Medical Spa',
    'Meditation Center',
    'Naturopath',
    'Nutritionist',
    'Reflexologist',
    'Disability Service',
    'Emergency Rescue Service',
    'Healthcare Administrator',
    'Home Health Care Service',
    'Mental Health Service',
    'Nursing Agency',
    'Orthotics & Prosthetics Service',
    'Reproductive Service',
    'Safety & First Aid Service',
    'Optician',
    'Pharmaceutical Company',
    'Pharmacy / Drugstore',
    'Medical Supply Store',
    'Vitamin Supplement Shop',
    'Therapist',
    'Counselor',
    'Family Therapist',
    'Marriage Therapist',
    'Occupational Therapist',
    'Physical Therapist',
    'Psychotherapist',
    'Sex Therapist',
    'Speech Pathologist',
    'Speech Therapist',
    'Sport Psychologist',
    'Health Spa',
    'Health Food Store',
    'Occupational Safety and Health Service',
    'Health Food Restaurant',
    'Sports & Recreation',
    'Sports & Fitness Instruction',
    'Boat / Sailing Instructor',
    'Coach',
    'Fitness Trainer',
    'Golf Instructor',
    'Horse Riding School',
    'Scuba Instructor',
    'Ski & Snowboard School',
    'Swimming Instructor',
    'Sports & Recreation Venue',
    'ATV Recreation Park',
    'Archery Range',
    'Badminton Court',
    'Baseball Field',
    'Basketball Court',
    'Batting Cage',
    'Bowling Alley',
    'Disc Golf Course',
    'Driving Range',
    'Equestrian Center',
    'Fencing Club',
    'Fitness Venue',
    'Boxing Studio',
    'Cycling Studio',
    'Dance Studio',
    'Fitness Boot Camp',
    'Gym/Physical Fitness Center',
    'Martial Arts School',
    'Pilates Studio',
    'Tai Chi Studio',
    'Yoga Studio',
    'Flyboarding Center',
    'Go-Kart Track',
    'Golf Course & Country Club',
    'Gun Range',
    'Gymnastics Center',
    'Hang Gliding Center',
    'Hockey Field / Rink',
    'Horseback Riding Center',
    'Ice Skating Rink',
    'Kiteboarding Center',
    'Laser Tag Center',
    'Miniature Golf Course',
    'Paddleboarding Center',
    'Paintball Center',
    'Public Swimming Pool',
    'Racquetball Court',
    'Rafting/Kayaking Center',
    'Recreation Center',
    'Rock Climbing Gym',
    'Rodeo',
    'Roller Skating Rink',
    'Rugby Pitch',
    'Scuba Diving Center',
    'Shooting/Hunting Range',
    'Skateboard Park',
    'Ski Resort',
    'Skydiving Center',
    'Soccer Field',
    'Squash Court',
    'Tennis Court',
    'Volleyball Court',
    'Sports League',
    'Amateur Sports League',
    'Esports League',
    'Professional Sports League',
    'School Sports League',
    'Sports Team',
    'Amateur Sports Team',
    'Esports Team',
    'Professional Sports Team',
    'School Sports Team',
    'Stadium, Arena & Sports Venue',
    'Baseball Stadium',
    'Basketball Stadium',
    'Cricket Ground',
    'Football Stadium',
    'Hockey Arena',
    'Rugby Stadium',
    'Soccer Stadium',
    'Tennis Stadium',
    'Track Stadium',
    'Sports Club',
    'Sports',
    'Sport',
    'Sport Team',

]
poverty = [
    # 'Non-Governmental Organization (NGO)',
    'Charity Organization',
    'Household Supplies',
    'Home Improvement',
    'Wholesale & Supply Store',
    'Furniture Repair & Upholstery Service',
    'Apparel & Clothing',
    'Arts & Humanities Website',
    'Mattress Store',
    'Charity Organization',
    'Clothing Company',
    'Office Supplies',
    'Home Improvement Service',
    'Nonprofit Organization',
    # 'Community'
]
food = [
    # 'Non-Governmental Organization (NGO)',
    'Cafeteria',
    'Kitchen/Cooking',
    'Vitamins/Supplements',
    'Food & Beverage',
    'Food Consultant',
    'Food Stand',
    'Food Delivery Service',
    'Food Wholesaler',
    'Food Truck',
    'Organic Grocery Store',
    'Specialty Grocery Store',
    'Farm',
    'Foodservice Distributor',
    'Charity Organization',
    'Food',
    'Diner',
    'Food & Beverage Company',
    'Foodservice Distributor',
    'Grocery Store',
    'Ethnic Grocery Store',
    'Fish Market',
    'Fruit & Vegetable Store',
    'Health Food Store',
    'Food Bank',
    'Food Website',
    'Organic Grocery Store',
    'Specialty Grocery Store',
    'Supermarket',
    'Wholesale Grocer',
    'Agriculture',
    'Agricultural Cooperative',
    'Agricultural Service',
    'Farm',
    'Personal Chef',
    'Restaurant',
    'Dairy Farm',
    'Fish Farm',
    'Livestock Farm',
    'Poultry Farm',
    'Urban Farm',

]
education = ['Medical School',
             'Art School',
             'Magazine',
             'Day Care',
             'Writer',
             'School',
             'Book & Magazine Distributor',
             'Book',
             'College / University Bookstore',
             'High School',
             'Books & Magazines',
             'Museum',
             'Trade School',
             'Book Series',
             'Public School',
             'Private School',
             'Library',
             'Course',
             'Performing Arts School',
             'Preschool',
             'Middle School',
             'School Sports Team',
             'Elementary School',
             'Education/Work status',
             'Martial Arts School',
             'Monument',
             'Traffic School',
             'Junior High School',
             'Comic Bookstore',
             'Cooking School',
             'Educational Research Center',
             'Academic Camp',
             'Religious School',
             'Archaeological Service',
             'Community College',
             'Public School',
             'Educational Consultant',
             'Aviation School',
             'Bartending School',
             'Computer Training School',
             'First Aid Class',
             'Flight School',
             'Massage School',
             'Music Lessons & Instruction School',
             'Nursing School',
             'Test Preparation Center',
             'Tutor/Teacher',
             'Literary Editor',
             'Cosmetology School',
             'Specialty School',
             'Driving School',
             'History Museum',
             'Dance School',
             'Language School',
             'Charity Organization',
             'College & University',
             'Bookstore',
             'Education/Work status',
             'Foodservice Distributor',
             'Education Website',
             'Personal Blog',
             'College & University',
             'Nonprofit Organization',
             'Book Genre',
             'Book Series',
             'Independent Bookstore',
             'Religious Bookstore',
             'Education', ]
environment = ['Solar Energy Service',
               'Environmental Conservation Organization',
               'Environmental Service',
               'Environmental Consultant',
               'Pet Cemetery',
               'Pet Supplies',
               'Janitorial Service',
               'Florist',
               'Petting Zoo',
               'Zoo',
               'Pet Cafe',
               'Pet Service',
               'Animal Shelter',
               'Dog Day Care Center',
               'Dog Trainer',
               'Dog Walker',
               'Horse Trainer',
               'Kennel',
               'Livery Stable',
               'Pet Adoption Service',
               'Pet Breeder',
               'Pet Groomer',
               'Pet Sitter',
               'Taxidermist',
               'Veterinarian',
               'Pet Store',
               'Aquatic Pet Store',
               'Reptile Pet Store',
               'Water Park',
               'Dog Park',
               'Outdoor Recreation',
               'Fairground',
               'Geographical Place',
               'Bay',
               'Beach',
               'Cape',
               'Cave',
               'Continent',
               'Desert',
               'Fjord/Loch',
               'Glacier',
               'Hot Spring',
               'Island',
               'Lake',
               'Mountain',
               'Ocean',
               'Pond',
               'Reservoir',
               'River',
               'Volcano',
               'Waterfall',
               'Park',
               'Arboretum',
               'Dog Park',
               'Field',
               'National Forest',
               'National Park',
               'Nature Preserve',
               'Picnic Ground',
               'Playground',
               'Public Square / Plaza',
               'state Park',
               'Public Garden',
               'Botanical Garden',
               'Community Garden',
               'Rose Garden',
               'Sculpture Garden',
               'Recreation Spot',
               'Bike Trail',
               'Diving Spot',
               'Fishing Spot',
               'Hiking Trail',
               'Rock Climbing Spot',
               'Snorkeling Spot',
               'Surfing Spot',

               ]
economy = [
    'Information Technology Company',
    'Automotive, Aircraft & Boat',
    'Automotive Dealership',
    'ATV Dealership',
    'Aircraft Dealership',
    'Organization',
    'Entrepreneur',
    'Automotive Wholesaler',
    'Boat Dealership',
    'Car Dealership',
    'Commercial Truck Dealership',
    'Golf Cart Dealership',
    'Motorcycle Dealership',
    'Recreational Vehicle Dealership',
    'Trailer Dealership',
    'Automotive Service',
    'Auto Detailing Service',
    'Automotive Body Shop',
    'Automotive Consultant',
    'Automotive Customization Shop',
    'Automotive Glass Service',
    'Automotive Leasing Service',
    'Automotive Repair Shop',
    'Automotive Restoration Service',
    'Automotive Shipping Service',
    'Automotive Storage Facility',
    'Automotive Wheel Polishing Service',
    'Automotive Window Tinting Service',
    'Boat Service',
    'Car Wash',
    'Emergency Roadside Service',
    'Gas station',
    'Marine Service station',
    'Motorcycle Repair Shop',
    'Oil Lube & Filter Service',
    'RV Repair Shop',
    'Smog Emissions Check station',
    'Tire Dealer & Repair Shop',
    'Towing Service',
    'Truck Repair Shop',
    'Wheel & Rim Repair Service',
    'Automotive Store',
    'Automotive Parts Store',
    'Car Stereo Store',
    'Marine Supply Store',
    'Motorsports Store',
    'Aviation Repair station',
    'Avionics Shop',
    'Motor Vehicle Company',
    'Automotive Manufacturer',
    'Motorcycle Manufacturer',
    'Finance',
    'Bank',
    'Commercial Bank',
    'Credit Union',
    'Investment Bank',
    'Retail Bank',
    'Financial Service',
    'Accountant',
    'Bank Equipment & Service',
    'Cash Advance Service',
    'Collection Agency',
    'Credit Counseling Service',
    'Currency Exchange',
    'Financial Aid Service',
    'Financial Consultant',
    'Financial Planner',
    'Franchise Broker',
    'Loan Service',
    'Tax Preparation Service',
    'Insurance Company',
    'Insurance Agent',
    'Insurance Broker',
    'Investing Service',
    'Brokerage Firm',
    'Investment Management Company',
    'Hedge Fund',
    'Science, Technology & Engineering',
    'Aerospace Company',
    'Biotechnology Company',
    'Chemical Company',
    'Gas & Chemical Service',
    'Petroleum Service',
    'Engineering Service',
    'Information Technology Company',
    'Computer Company',
    'Electronics Company',
    'Internet Company',
    'Software Company',
    'Telecommunication Company',
    'Cable & Satellite Company',
    'Robotics Company',
    'Solar Energy Company',
    'Structural Engineer',
    'Surveyor',
    'Commercial & Industrial',
    'Automation Service',
    'Commercial & Industrial Equipment Supplier',
    'Environmental Service',
    'Environmental Consultant',
    'Geologic Service',
    'Occupational Safety and Health Service',
    'Recycling Center',
    'Forestry & Logging',
    'Forestry Service',
    'Logging Contractor',
    'Hotel Services Company',
    'Industrial Company',
    'Inventory Control Service',
    'Manufacturer/Supplier',
    'Aircraft Manufacturer',
    'Apparel Distributor',
    'Appliance Manufacturer',
    'Bags & Luggage Company',
    'Clothing Company',
    'Glass Manufacturer',
    'Business & Economy Website',
    'Jewelry & Watches Company',
    'Jewelry Wholesaler',
    'Machine Shop',
    'Mattress Manufacturer',
    'Metal & Steel Company',
    'Metal Fabricator',
    'Metal Plating Service Company',
    'Metal Supplier',
    'Mining Company',
    'Granite & Marble Supplier',
    'Plastic Company',
    'Plastic Fabricator',
    'Plastic Manufacturer',
    'Textile Company',
    'Tobacco Company',
]
cultural = [
    'Cultural Center',
    'Cultural Gifts Store',
    'Society & Culture Website',
    'Museum',
    'Art'
    'Art Museum',
    'Asian Art Museum',
    'Regional Website',
    'Cartooning Museum',
    'Contemporary Art Museum',
    'Costume Museum',
    'Decorative Arts Museum',
    'Design Museum',
    'Modern Art Museum',
    'Photography Museum',
    'Textile Museum',
    'Aviation Museum',
    'Children\'s Museum',
    'History Museum',
    'Civilization Museum',
    'Community Museum',
    'Science Museum',
    'Computer Museum',
    'Observatory',
    'Planetarium',
    'Sports Museum',
    'Public Figure',
    'Actor',
    'Artist',
    'Author',
    'Blogger',
    'Dancer',
    'Designer',
    'Fashion Designerv',
    'Digital Creator',
    'Editor',
    'Fashion Model',
    'Film Director',
    'Fitness Model',
    'Motivational Speaker',
    'News Personality',
    'Orchestra',
    'Producer',
    'Scientist',
    'Spiritual Leader',
    'Talent Agent',
    'Writer',
]
policy = [
    'Government Organization',
    'Legal',
    'Journalist',
    'Armed Forces',
    'Blogger',
    'Political Party',
    'Politician',
    'Lawyer & Law Firm',
    'Bankruptcy Lawyer',
    'Contract Lawyer',
    'Corporate Lawyer',
    'Criminal Lawyer',
    'DUI Lawyer',
    'Divorce & Family Lawyer',
    'Entertainment Lawyer',
    'Estate Planning Lawyer',
    'General Litigation',
    'Immigration Lawyer',
    'Intellectual Property Lawyer',
    'Internet Lawyer',
    'Juvenile Lawyer',
    'Labor & Employment Lawyer',
    'Landlord & Tenant Lawyer',
    'Malpractice Lawyer',
    'Medical Lawyer',
    'Military Lawyer',
    'Personal Injury Lawyer',
    'Property Lawyer',
    'Real Estate Lawyer',
    'Tax Lawyer',
    'Legal Service',
    'Bail Bondsmen',
    'Lobbyist',
    'Notary Public',
    'Private Investigator',
    'Process Service',
    'Religious Organization',
    'Journalist',
    'Newspaper',
    'News & Media Website',
    'Armed Forces',

]
religious = [
    'Religious Place of Worship',
    'Assemblies of God',
    'Buddhist Temple',
    'Church',
    'African Methodist Episcopal Church',
    'Anglican Church',
    'Apostolic Church',
    'Baptist Church',
    'Catholic Church',
    'Charismatic Church',
    'Christian Church',
    'Christian Science Church',
    'Church of Christ',
    'Church of God',
    'Church of Jesus Christ of Latter-day Saints',
    'Congregational Church',
    'Eastern Orthodox Church',
    'Episcopal Church',
    'Evangelical Church',
    'Full Gospel Church',
    'Holiness Church',
    'Independent Church',
    'Interdenominational Church',
    'Lutheran Church',
    'Mennonite Church',
    'Methodist Church',
    'Nazarene Church',
    'Nondenominational Church',
    'Pentecostal Church',
    'Presbyterian Church',
    'Seventh Day Adventist Church',
    'Convent & Monastery',
    'Hindu Temple',
    'Kingdom Hall',
    'Mission',
    'Mosque',
    'Religious Center',
    'Sikh Temple',
    'Synagogue',
    'Religious Organization',
]
music = ['Musician',
         'Musician/Band',
         'Music',
         'Band']
"""
nlp = spacy.load('xx_ent_wiki_sm')


def remove_symbols(text):
    return re.sub(r'[^\x00-\x7F\x80-\xFF\u0100-\u017F\u0180-\u024F\u1E00-\u1EFF]', u'', text)


# Function to remove whitespace


def remove_whitespace(text):
    return " ".join(text.split())


def processing_text(text):
    doc = nlp(text + '\n')
    my_stop_words = [u'et', u'ça', u'se', u'dit', u'de', u'des', u'les', u'du', u'la', u'le', u'', u'à', u'And', u'un',
                     u'qui', u'pour', u'I', u'je', u'mon', u'but', u'you']
    for stopword in my_stop_words:
        lexeme = nlp.vocab[stopword]
        lexeme.is_stop = True
    # we add some words to the stop word list
    texts, data, skl_texts = [], [], []
    for w in doc:
        # if it's not a stop word or punctuation mark, add it to our article!
        if w.text != '\n' and not w.is_stop and not w.is_punct and not w.like_num and (w.text in nlp.vocab) and \
                w.is_alpha:
            # we add the lematized version of the word
            data.append(w.lemma_)
        # if it's a new line, it means we're onto our next document
        if w.text == '\n' and data:
            skl_texts.append(' '.join(data))
            texts.append(data)
            data = []
    bigram = gensim.models.Phrases(texts)
    texts = [bigram[line] for line in texts]
    dictionary = Dictionary(texts)
    corpus = [dictionary.doc2bow(text) for text in texts]
    dictionary = gensim.corpora.Dictionary(texts)
    # lsimodel = LsiModel(corpus=corpus, num_topics=10, id2word=dictionary)
    # print(lsimodel.show_topics(num_topics=5))
    # hdpmodel = HdpModel(corpus, id2word=dictionary)
    ldamodel = LdaModel(corpus, num_topics=4, id2word=dictionary, passes=10)

    print(ldamodel.print_topics())
    # print(hdpmodel.show_topics())


def my_thread2(name, data, user):
    collection = ""

    while True:
        try:
            # Retrieve one post
            for element in data['data']:
                try:
                    collection = collection + " " + (element['message'])
                except Exception as e:
                    pass
            data = requests.get(data['paging']['next']).json()
        except Exception as e:
            # no more pages, break the loop
            break
            print(e)
    processing_text(remove_symbols(collection))
"""


def my_thread(name, data, user):
    print(name)
    i = 5
    health_count = 0
    poverty_count = 0
    food_count = 0
    education_count = 0
    policy_count = 0
    religious_count = 0
    environment_count = 0
    cultural_count = 0
    others = 0
    economy_count = 0
    music_count = 0
    while i > 0:
        try:
            # Retrieve one post
            for element in data['data']:
                try:
                    health_test = poverty_test = food_test = education_test = policy_test = religious_test = \
                        environment_test = cultural_test = economy_test = music_test = False
                    for category in element['category_list']:
                        if (category['name'] in health) & (not health_test):
                            health_count += 1
                            health_test = True
                        elif (category['name'] in poverty) & (not poverty_test):
                            poverty_count += 1
                            poverty_test = True
                        elif (category['name'] in food) & (not food_test):
                            food_count += 1
                            food_test = True
                        elif (category['name'] in education) & (not education_test):
                            education_count += 1
                            education_test = True
                        elif (category['name'] in policy) & (not policy_test):
                            policy_count += 1
                            policy_test = True
                        elif (category['name'] in religious) & (not religious_test):
                            religious_count += 1
                            religious_test = True
                        elif (category['name'] in environment) & (not environment_test):
                            environment_count += 1
                            environment_test = True
                        elif (category['name'] in music) & (not music_test):
                            music_count += 1
                            music_test = True
                        elif (category['name'] in cultural) & (not cultural_test):
                            cultural_count += 1
                            cultural_test = True
                        elif (category['name'] in economy) & (not economy_test):
                            economy_count += 1
                            economy_test = True

                        else:
                            others += 1
                except Exception as e:
                    print(e)
            data = requests.get(data['paging']['next']).json()
            i = i - 1
        except Exception as e:
            # no more pages, break the loop
            print(e)
            break
    print("done: " + name)
    total = health_count + policy_count + food_count + education_count + policy_count + religious_count + \
            environment_count + cultural_count + music_count + economy_count
    tab = {'Health': health_count / total * 100,
           'Policy': policy_count / total * 100,
           'Economy': economy_count / total * 100,
           'Food': food_count / total * 100,
           'Education': education_count / total * 100,
           'Religious': religious_count / total * 100,
           'Environment': environment_count / total * 100,
           'Cultural': cultural_count / total * 100,
           'Music': music_count / total * 100,
           }
    """
    try:
        health_category, created = Category.objects.get_or_create(name='Health')
        policy_category, created = Category.objects.get_or_create(name='Policy')
        economy_category, created = Category.objects.get_or_create(name='Economy')
        food_category, created = Category.objects.get_or_create(name='Food')
        education_category, created = Category.objects.get_or_create(name='Education')
        religious_category, created = Category.objects.get_or_create(name='Religious')
        environment_category, created = Category.objects.get_or_create(name='Environment')
        cultural_category, created = Category.objects.get_or_create(name='Cultural')
        music_category, created = Category.objects.get_or_create(name='Music')

        stat1, created = CategoryUser.objects.get_or_create(user=user, category=health_category)
        stat1.percentage = tab['Health']
        stat2, created = CategoryUser.objects.get_or_create(user=user, category=policy_category)
        stat2.percentage = tab['Policy']
        stat3, created = CategoryUser.objects.get_or_create(user=user, category=economy_category)
        stat3.percentage = tab['Economy']
        stat4, created = CategoryUser.objects.get_or_create(user=user, category=food_category)
        stat4.percentage = tab['Food']
        stat5, created = CategoryUser.objects.get_or_create(user=user, category=education_category)
        stat5.percentage = tab['Education']
        stat6, created = CategoryUser.objects.get_or_create(user=user, category=religious_category)
        stat6.percentage = tab['Religious']
        stat7, created = CategoryUser.objects.get_or_create(user=user, category=environment_category)
        stat7.percentage = tab['Environment']
        stat8, created = CategoryUser.objects.get_or_create(user=user, category=cultural_category)
        stat8.percentage = tab['Cultural']
        stat9, created = CategoryUser.objects.get_or_create(user=user, category=music_category)
        stat9.percentage = tab['Music']
        stats = [stat9, stat8, stat7, stat6, stat5, stat4, stat3, stat2, stat1]
        for item in stats:
            item.save()
    except():
        pass
        
    interests = CategoryUser.objects.filter(user=user)
    results = {}
    for stat in interests:
        results[stat.category.name] = stat.percentage
        """
    return {'result': tab
            }


def preferences(user):
    try:
        token = user.facebook.fb_token
    except ObjectDoesNotExist:
       print("There is no FacebookAccount here.")
    if token != '':
        try:
            graph = facebook.GraphAPI(token)
            fields = ['likes.limit(100){category_list,name}, posts.limit(200)']
            profile = graph.get_object('me', fields=fields)
            pool = ThreadPool(processes=1)
            thread7 = pool.apply_async(my_thread, args=('likes', profile['likes'], user))
            result = thread7.get()
            return result
        except Exception as e:
            # no more pages, break the loop
            print(e)
            return {'error': 'Error fetching data from Facebook'}
    else:
        categories = Category.objects.all()
        if len(categories) == 0:
            return {'error': 'User got no preferences'}
        results = {}
        for item in categories:
            try:
                stat = CategoryUser.objects.get(user=user, Category=item)
                results[stat.category.name] = stat.percentage
            except Exception as e:
                print(e)
                return {'error': 'User got no preferences'}
        return results
