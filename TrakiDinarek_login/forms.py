import re

from django import forms
from django.core.validators import RegexValidator
from django.forms import MultipleChoiceField, models
from phonenumber_field.formfields import PhoneNumberField
from django.contrib.auth import authenticate

from TrakiDinarek_login.models import User, Category, UserProfile


class CustomModelChoiceIterator(models.ModelChoiceIterator):
    def choice(self, obj):
        return (self.field.prepare_value(obj),
                self.field.label_from_instance(obj), obj)


class CustomModelChoiceField(models.ModelMultipleChoiceField):
    def _get_choices(self):
        if hasattr(self, '_choices'):
            return self._choices
        return CustomModelChoiceIterator(self)

    choices = property(_get_choices,
                       MultipleChoiceField._set_choices)


class SignUpForm(forms.Form):
    first_name = forms.CharField(required=True, label='First name',
                                 widget=forms.TextInput(attrs={'placeholder': 'First name'}))
    last_name = forms.CharField(required=True, label='Last name',
                                widget=forms.TextInput(attrs={'placeholder': 'Last name'}))
    phone_number = PhoneNumberField(required=False,
                                    label='Phone',
                                    widget=forms.TextInput(attrs={'placeholder': '+216 11 111 111'}))
    password = forms.CharField(widget=forms.PasswordInput(), required=False, min_length=8)
    confirm_password = forms.CharField(widget=forms.PasswordInput(), required=False, min_length=8)
    email = forms.EmailField(required=True, widget=forms.EmailInput(attrs={'placeholder': 'example@example.com'}))
    address = forms.CharField(required=False,
                              widget=forms.TextInput(attrs={'placeholder': 'Enter your address'}))
    city = forms.CharField(required=False,
                           max_length=30,
                           widget=forms.TextInput(attrs={'placeholder': 'Enter your city'}))
    photo = forms.ImageField(required=False)
    zip = forms.CharField(required=False,
                          max_length=8,
                          widget=forms.TextInput(attrs={'placeholder': 'Enter your zip code'}))

    favorite_topics = CustomModelChoiceField(required=False,
                                             widget=forms.CheckboxSelectMultiple,
                                             queryset=Category.objects.all())

    def clean(self):
        cleaned_data = super(SignUpForm, self).clean()
        password = cleaned_data.get("password")
        confirm_password = cleaned_data.get("confirm_password")

        if password != confirm_password:
            raise forms.ValidationError(
                "password and confirm_password does not match"
            )
        if User.objects.filter(email=cleaned_data['email']).exists():
            raise forms.ValidationError(
                "email already exist"
            )
        if UserProfile.objects.filter(phone=cleaned_data['phone_number']).exists():
            raise forms.ValidationError(
                "phone_number already used"
            )


class LoginForm(forms.Form):
    email = forms.EmailField(required=True, label='Email',
                             widget=forms.TextInput(attrs={'placeholder': 'example@example.com',
                                                           'class': 'form-control'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'Your Password',
                                                                 'class': 'form-control'}),
                               required=True, min_length=8)
    remember_me = forms.BooleanField(label='Remember me', required=False)

    def clean(self):
        super(LoginForm, self).clean()
        email = self.cleaned_data['email']
        password = self.cleaned_data['password']
        global exists
        try:
            user = User.objects.get(email=email)
            if user is not None:
                exists = True
            user = authenticate(email=email, password=password)
            if user is not None:  # to check whether user is available or not?
                # the password verified for the user
                if user.is_active:
                    print("User is valid, active and authenticated")

                else:
                    pass
            else:
                # the authentication system was unable to verify the username and password
                if exists:
                    raise forms.ValidationError("The password you’ve entered is incorrect. ")
                else:
                    raise forms.ValidationError("The email you’ve entered doesn't match any account. ")
        except User.DoesNotExist:
            raise forms.ValidationError("The email you’ve entered doesn't match any account. ")

        return self.cleaned_data

    def clean_password(self):
        password = self.cleaned_data['password']
        if len(password) == 0:
            raise forms.ValidationError("password minimum length is 8.")
        return password


class SendEmailForm(forms.Form):
    email = forms.EmailField(required=True, label='Email',
                             widget=forms.TextInput(attrs={'placeholder': 'example@example.com',
                                                           'class': 'form-control'}))

    def clean_email(self):
        value = self.cleaned_data['email']
        try:
            user = User.objects.get(email=value)
            if user is None:
                raise forms.ValidationError("This email doesn't exist")
            return value
        except User.DoesNotExist:
            raise forms.ValidationError("The email you’ve entered doesn't match any account. ")


class ResetPasswordForm(forms.Form):
    password = forms.CharField(widget=forms.PasswordInput(), required=True, min_length=8,
                               validators=[RegexValidator(
                                   regex=r'(?=^.{8,}$)(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s)[0-9a-zA-Z!@#$%^&*()]*$',
                                   message='The password should meet some minimum requirements, at least one '
                                           'lower-case character, at least one digit, Allowed Characters: A-Z a-z 0-9 '
                                           '@ * _ - . !',
                                   code='invalid_password'
                               ), ])
    confirm_password = forms.CharField(widget=forms.PasswordInput(), required=True, min_length=8)

    def clean(self):
        super(ResetPasswordForm, self).clean()
        password = self.cleaned_data.get("password")
        confirm_password = self.cleaned_data.get("confirm_password")
        if password is not None:
            if password != confirm_password:
                raise forms.ValidationError(
                    "password and confirm_password does not match"
                )


class ActivateForm(forms.Form):
    email = forms.EmailField(required=True)

    def clean(self):
        cleaned_data = super(ActivateForm, self).clean()
        if not User.objects.filter(email=cleaned_data['email']).exists():
            raise forms.ValidationError(
                "email doesn't exist"
            )