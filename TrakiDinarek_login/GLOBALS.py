import re
from django.core.exceptions import ValidationError


def password_validator(val):
    reg = re.compile(
        r'^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$')
    if not reg.match(val):
        raise ValidationError(u'%s hashtag doesnot comply' % val)


