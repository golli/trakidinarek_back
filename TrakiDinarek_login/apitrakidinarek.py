import jwt
from django.contrib.auth import authenticate, user_logged_in
from django.contrib.contenttypes.models import ContentType
from django.http import HttpResponse, JsonResponse
from django.utils.http import urlsafe_base64_decode

from rest_framework import generics, viewsets
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.response import Response
from rest_framework.status import HTTP_404_NOT_FOUND, HTTP_200_OK
from rest_framework.views import APIView
from rest_framework_jwt.serializers import jwt_payload_handler

from DataMining import settings
from TrakiDinarek_login.facebook_graph import preferences
from .models import User, CategoryUser, Category
from .serializers import UserSerializer, CreateUserSerializer

from .tokens import account_activation_token


class UserCreate(generics.CreateAPIView):
    permission_classes = ()
    authentication_classes = ()
    serializer_class = CreateUserSerializer


class UserList(generics.ListAPIView):
    """
    Provides a get method handler.
    returns all instances
    """
    permission_classes = (IsAuthenticated,)

    queryset = User.objects.all()
    serializer_class = UserSerializer


class UserDetail(generics.RetrieveDestroyAPIView):
    """
    Provides a get method handler.
    returns Users details by ID : Admin method
    """
    permission_classes = (IsAdminUser,)

    queryset = User.objects.all()
    serializer_class = UserSerializer


class UserProfile(generics.RetrieveUpdateAPIView):
    """
    Provides a get method handler.
    returns Users details
    and Update user if put
    """
    permission_classes = (IsAuthenticated,)
    serializer_class = UserSerializer

    def get(self, request, *args, **kwargs):
        serializer = self.serializer_class(request.user)

        return Response(serializer.data, status=HTTP_200_OK)

    def put(self, request, *args, **kwargs):
        user = request.user
        serializer = UserSerializer(user, request.data, partial=True)
        if serializer.is_valid():
            instance = serializer.save()
            # Generate a new token
            payload = jwt_payload_handler(instance)
            token = jwt.encode(payload, settings.SECRET_KEY)
            return Response({'token': token,
                             'user': serializer.data
                             }
                            , status=HTTP_200_OK)
        else:
            response = JsonResponse({'errors': serializer.errors})
            response.status = 500
            return response

    def delete(self, request, *args, **kwargs):
        user = request.user
        user.is_active = 0
        user.save()
        return Response({'user disabled successfully'})


class Login(APIView):
    permission_classes = ()

    def post(self, request, *args, **kwargs):
        if not request.data:
            return Response({'Error': "Please provide email/password"}, status="400")

        email = request.data['email']
        password = request.data['password']
        try:
            user = authenticate(email=email, password=password)
            if user is not None:  # to check whether user is available or not?
                # the password verified for the user
                if user.is_active:
                    print("User is valid, active and authenticated")
                    user_logged_in.send(sender=user.__class__,
                                        request=request, user=user)
                    payload = jwt_payload_handler(user)
                    token = jwt.encode(payload, settings.SECRET_KEY)
                    return Response({'token': token,
                                     'id': user.id,
                                     'username': (user.first_name + " " + user.last_name),
                                     'email': user.email}, status=HTTP_200_OK)
                else:
                    print("The password is valid, but the account has been disabled!")
                    return Response({'error': 'account not activated '}, status=HTTP_404_NOT_FOUND)
            else:
                # the authentication system was unable to verify the username and password
                print("The username and password were incorrect.")
                return Response({'error': 'Invalid Credentials'}, status=HTTP_404_NOT_FOUND)

        except User.DoesNotExist:
            return Response({'Error': "Invalid username/password"}, status="400")


class Preferences(APIView):
    """
       Provides a get method to returns the user's preferences,Update if there is a valid facebook token
       Provides a post method to update preferences manually,
       """
    permission_classes = (IsAuthenticated,)
    serializer_class = UserSerializer

    def get(self, request, *args, **kwargs):
        # serializer to handle turning our `User` object into something that
        # can be JSONified and sent to the client.
        user = request.user

        return Response(preferences(user))

    def post(self, request, *args, **kwargs):
        if not request.data['categories']:
            return Response({'Error': "Please provide user's preferences"}, status="400")
        results = {}
        user = request.user
        interests = dict(request.data)['categories']
        health_category, created = Category.objects.get_or_create(name='Health')
        if created:
            Category.objects.get_or_create(name='Policy')
            Category.objects.get_or_create(name='Economy')
            Category.objects.get_or_create(name='Food')
            Category.objects.get_or_create(name='Education')
            Category.objects.get_or_create(name='Religious')
            Category.objects.get_or_create(name='Environment')
            Category.objects.get_or_create(name='Cultural')
            Category.objects.get_or_create(name='Music')
        categories = Category.objects.all()
        for item in categories:
            stat, created = CategoryUser.objects.get_or_create(user=user, category=item)
            if item.name in interests:
                stat.percentage = 100 / len(interests)
            else:
                stat.percentage = 0

            stat.save()
            results[stat.category.name] = stat.percentage

        return Response({'result': results
                         }
                        )


class Activate(APIView):
    permission_classes = ()
    authentication_classes = ()

    def get(self, request, uidb64, token):
        try:
            uid = urlsafe_base64_decode(uidb64).decode()
            user = User.objects.get(pk=uid)
        except(TypeError, ValueError, OverflowError, User.DoesNotExist):
            user = None
        if user is not None and account_activation_token.check_token(user, token):
            user.is_active = True
            user.save()
            payload = jwt_payload_handler(user)
            token = jwt.encode(payload, settings.SECRET_KEY)

            return Response({'message': 'Thank you for your email confirmation. Now you can login your account.',
                             'token': token
                             }
                            )
        else:
            return HttpResponse('Activation link is invalid!')

"""
class AssociationCreateUpdate(generics.CreateAPIView):
    permission_classes = (IsAuthenticated,)

    def put(self, request, *args, **kwargs):
        user = request.user
        try:
            user.associationdirector
            serializer = AssociationSerializer(user.associationdirector,request.data,partial=True)
            return Response({
                'Association': serializer.data
            })
        except Exception as e:
            user.__class__ = AssociationDirector
            user.polymorphic_ctype = ContentType.objects.get_for_model(user,
                                                                       for_concrete_model=False)
            user.id_association = request.data['id_association']
            user.jort_justification = request.data['jort_justification']
            user.president_justification = request.data['president_justification']
            user.declaration_justification = request.data['declaration_justification']
            user.save(force_insert=True)
            print(e)
        return Response({'id_association': user.id_association,
                         'jort_justification': user.jort_justification.url,
                         'president_justification': user.president_justification.url,
                         'declaration_justification': user.declaration_justification.url
                         }, status=HTTP_200_OK

                        )
"""