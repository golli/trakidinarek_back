from django import template

from DataMining.settings import MEDIA_URL
from TrakiDinarek_login.models import User

register = template.Library()


@register.simple_tag
def user_picture(user: User):
    try:
        if not user.profile.photo is None and user.profile.photo != "":
            return MEDIA_URL + '/' + user.profile.photo
        elif not user.facebook.image_url is None:
            return user.facebook.image_url
        else:
            return MEDIA_URL + "/Images/UsersPictures/default-profile.jpeg"

    except:
        return MEDIA_URL + "/Images/UsersPictures/default-profile.jpeg"


@register.simple_tag
def position(user: User):
    try:
        return user.Director.name + " Director"
    except:
        return " "


@register.simple_tag
def user_phone_number(user: User):
    try:
        if not user.profile.phone is None:
            return "Phone: " + str(user.profile.phone)
        else:
            return ""
    except:
        return ""


@register.simple_tag
def user_address(user: User):
    address = ""
    if not user.profile.address is None:
        address += user.profile.address + "<br/>"
    if not user.profile.city is None:
        address += user.profile.city + "<br/>"
    if not user.profile.zip is None:
        address += user.profile.zip + "<br/>"
    return address


@register.simple_tag
def filename(path):
    try:
        if path is None:
            return ""
        else:
            path = str(path).split("/")
            return path[-1]
    except:
        return ""
