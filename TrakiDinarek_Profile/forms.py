from django import forms
from django.forms import ModelForm
from django.template.defaultfilters import filesizeformat
from nltk.lm.vocabulary import _
from phonenumber_field.formfields import PhoneNumberField

from TrakiDinarek_login.forms import CustomModelChoiceField
from TrakiDinarek_login.models import UserDocuments, Category


class PersonalInformation(forms.Form):
    first_name = forms.CharField(required=True, label='First name',
                                 widget=forms.TextInput(attrs={'placeholder': 'First name'}))
    last_name = forms.CharField(required=True, label='Last name',
                                widget=forms.TextInput(attrs={'placeholder': 'Last name'}))
    phone_number = PhoneNumberField(required=False,
                                    label='Phone',
                                    widget=forms.TextInput(attrs={'placeholder': '+216 11 111 111'}))
    password = forms.CharField(widget=forms.PasswordInput(), required=False, min_length=8)
    confirm_password = forms.CharField(widget=forms.PasswordInput(), required=False, min_length=8)
    email = forms.EmailField(required=True, widget=forms.EmailInput(attrs={'placeholder': 'example@example.com'}))
    address = forms.CharField(required=False,
                              widget=forms.TextInput(attrs={'placeholder': 'Enter your address'}))
    city = forms.CharField(required=False,
                           max_length=30,
                           widget=forms.TextInput(attrs={'placeholder': 'Enter your city'}))
    photo = forms.ImageField(required=False)
    zip = forms.CharField(required=False,
                          max_length=8,
                          widget=forms.TextInput(attrs={'placeholder': 'Enter your zip code'}))
    favorite_topics = CustomModelChoiceField(required=False,
                                             widget=forms.CheckboxSelectMultiple,
                                             queryset=Category.objects.all())

    def __init__(self, *args, **kwargs):
        super(PersonalInformation, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            if visible.name != "photo":
                visible.field.widget.attrs['class'] = 'form-control'


class IdentificationInformation(ModelForm):
    class Meta:
        model = UserDocuments
        fields = ['jort_justification', 'president_justification', 'declaration_justification']

    def __init__(self, *args, **kwargs):
        super(IdentificationInformation, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'default'

    def clean_content(self):
        content = self.cleaned_data['content']
        content_type = content.content_type.split('/')[0]
        if content_type in ['image', 'pdf']:
            if content._size > 5242880:
                raise forms.ValidationError(_('Please keep file_size under %s. Current file_size %s') %
                                            (filesizeformat(5242880), filesizeformat(content._size)))
        else:
            raise forms.ValidationError(_('File type is not supported'))
        return content
