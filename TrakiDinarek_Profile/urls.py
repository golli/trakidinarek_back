from django.urls import path

from TrakiDinarek_Profile import views

urlpatterns = [
    path("logout/", views.logout_user, name='logout_user'),  # logout user
    path("home", views.index, name="homepage"),  # home page
    path("profile/", views.profile, name='profile'),  # logout user

]
