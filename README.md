# Crowdfunding for Charity Projects Web Application

This work is part of the final project of studies at the Higher Institute of Applied Science and Technologies of Sousse. This project was conducted in Digital Innovation in order to implement a web application of crowdfunding for charity projects.

The aim of this project is to develop a web application that allows users to donate to various charity projects through a crowdfunding platform. The web application will use web services and Blockchain technology to ensure secure transactions and transparency in the donation process.

## Tech Stack

The following technologies were used in the development of this project:

- Django
- web3.py
- Solidity
- JavaScript
- Ethereum wallets
- HTML
- Python
- Operating System: Ubuntu

## Thesis Report

You can find the thesis report for this project [here](https://drive.google.com/file/d/1uXmQUcyXfCam42acleMr7xBtoP3BchxA/view?usp=sharing).

## Demo Video

You can watch a demo video of the web application [here](https://drive.google.com/file/d/1GdD2ivY87JHC_YpmFNDmT99Q01tCf2E1/view?usp=share_link).

## Presentation

You can view the presentation for this project [here](https://docs.google.com/presentation/d/1awBvd5ADSSuCTnpmDGtVlfVOGmR7b0Dr/edit?usp=share_link&ouid=103676438667177656103&rtpof=true&sd=true).
