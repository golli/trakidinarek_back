from django.urls import path

from TrakiDinarek_Admin import views

urlpatterns = [
    path("users/", views.get_users, name='return_users'),  # asscociation management
    path("associations/", views.get_associations, name='return_associations'),  # association management
    path("home_edit/", views.get_sliders, name='home_slider'),  # Home management
    path("user/<str:user_id>/", views.get_user, name='get_user'),
]
