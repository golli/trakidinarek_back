from django.forms import ModelForm

from TrakiDinarek_Admin.models import Slider


class AddSlider(ModelForm):
    class Meta:
        model = Slider
        fields = ['name', 'short_description', 'sub_name', 'description', 'photo']

    def __init__(self, *args, **kwargs):
        super(AddSlider, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = "form-control"
